import os

import numpy as np

from fs_code import flightstream_commands as fc
from fs_code import flightstream_project as fs_p
from fs_code import utils, utils_command
from fs_code.read_csv_flug import recursive_path_join

# FLIGHTSTREAM_PATH = "flightstream_files/FlightStream.lnk"
RPM2RAD = 0.10472
# TODO find a solution (flightstream problem more than code in itself)
UNITE_BROKEN = True


def write_script(filename, commands):
    with open(filename, "w+") as f:
        for cmd in commands:
            f.write(cmd)
    if f.closed != True:
        print("Warning: script not correctly closed")


def pitch_rotation(rotors):
    l = []
    for rotor in rotors:
        for surface in rotor.surfaces:
            if not surface.angle == 0 and not surface.index in fs_p.HUB_SURFACE:
                l.append(fc.surface_rotate(rotor.frame_id, rotor.pitch_axis,
                                           surface.angle, surface.index, True))
    return l


def rotation(rotors, aoa_angle, aoa_axis="Y",):
    """Hypothesis rotation aoa_axis common with the frame axis """
    l = []
    l.append(fc.create_new_coordinate_system())
    origin_frame_id = 1
    rotation_frame = 2  # TODO change this, maybe class
    l.append(fc.rotate_coordinate_system(
        aoa_angle, origin_frame_id, rotation_frame, aoa_axis))
    for rotor in rotors:
        for surface in rotor.surfaces:
            l.append(fc.surface_rotate(
                rotation_frame, aoa_axis, aoa_angle, surface.index, True))
    return l, rotation_frame


def set_free_stream(steady: bool, rotor: utils.Rotor):
    """ For steady implied all rotor have the same rpm and rotation axis"""
    cmd = ""
    if steady:  # and not free_stream_rotational_dict:
        free_stream_rotational_dict = {
            "RPM": rotor.rpm, "FRAME": 1, "AXIS": rotor.rotation_axis}
        cmd = fc.edit_freestream("ROTATIONAL", free_stream_rotational_dict)
    elif not steady:
        cmd = fc.edit_freestream("CONSTANT", {})
    else:
        print("incorrect arguments")
        raise Exception
    return cmd


def set_solver(steady: bool, set_solver_dict):
    cmd = ""
    if not set_solver_dict:
        print("empty dictionnary")
        raise Exception
    elif steady:
        cmd = fc.set_solver("ROTARY", set_solver_dict)
    else:
        cmd = fc.set_solver("UNSTEADY", set_solver_dict)
    return cmd


def initialize_solver(rotors, periodic=0):
    symmetry_type = "NONE"
    symmetry_periodicity = 0
    if periodic > 0:
        symmetry_type = "PERIODIC"
        symmetry_periodicity = periodic
    surfaces = []
    for rotor in rotors:
        for surface in rotor.surfaces:
            surfaces.append(surface.index)
    return fc.initialize_solver(surfaces, symmetry_type=symmetry_type, symmetry_periodicity=symmetry_periodicity, stabilization_strength=1)


def steady_script(dirname, script_file, flightstream_file, output_file, img_path, rotors, free_stream_velocity, periodic: int, ref_length, aoa, ref_area):
    set_solver_dict = {"BLOCK_ITERATIONS": 10,
                       "INDUCED_VELOCITY_RESIDUAL": 10.0}
    steady = True
    max_rpm = 0
    for rotor in rotors:
        if max_rpm < rotor.rpm:
            max_rpm = rotor.rpm
    general_script(dirname, script_file, flightstream_file, output_file,
                   steady, rotors, set_solver_dict, free_stream_velocity, periodic, img_path=img_path, ref_length=ref_length, aoa=aoa, rpm=max_rpm, ref_area=ref_area)


def unsteady_script(dirname, script_file, flightstream_file, output_file, img_path, rotors, free_stream_velocity, ref_length, angle_dt, aoa, ref_area, h_space=fs_p.H_SPACE):
    max_rpm = 0
    min_rpm = np.infty
    for rotor in rotors:
        if max_rpm < abs(rotor.rpm):
            max_rpm = abs(rotor.rpm)
        if min_rpm > abs(rotor.rpm):
            min_rpm = abs(rotor.rpm)
    dt = 0.0003
    if max_rpm > 0:
        dt = round(1/(np.rad2deg(max_rpm*RPM2RAD)) * angle_dt, 4)
        if dt < 0.0001:  # minimal value of Flightstream
            dt = 0.0001
    # for only one revolution, or add number of revolution as parameters ?
    time_iterations = int(round(2*np.pi / ((min_rpm*RPM2RAD)*dt), 0))
    # approximation of time needed to the airflow comes from the first rotor to the second rotor
    time_iterations += int(round((h_space/fs_p.H_SPACE) * (30/(dt/0.0001)), 0))
    print((h_space/fs_p.H_SPACE))
    print(int(round((h_space/fs_p.H_SPACE) * (30/(dt/0.0001)), 0)))
    print("time it: ", time_iterations)
    # if time_iterations < 60: # precaution?
    #     time_iterations = 60
    # for angle_dt° rotation
    # print("time it: ", time_iterations)
    set_solver_dict = {"TIME_ITERATIONS": time_iterations,  # TODO estimate the number of iterations depending on the space between the two rotors that is not count actually (one/two rotations and should be one rotation after of the upcoming Upper stream to the lower rotor)
                       "DELTA_TIME": dt,
                       "ACTIVE_SCRIPT": "DISABLE"}
    steady = False
    periodic = 0
    general_script(dirname, script_file, flightstream_file, output_file,
                   steady, rotors, set_solver_dict, free_stream_velocity, periodic, ref_length, aoa, img_path=img_path, rpm=max_rpm, ref_area=ref_area, h_space=h_space)


def general_script(dirname, script_file, flightstream_file, output_file, steady, rotors, set_solver_dict,  freestream_velocity, periodic: int, ref_length=10.0, angle=0, aoa={"angle": 0, "axis": "Y"}, img_path="", rpm=1200, ref_area=3.281, h_space=fs_p.H_SPACE):
    wake_size = 300
    mini_cp = -100.0
    iterations = 150
    processors = 4

    beta = 0  # not relevant to change (axis symmetry)
    # depend from the rotor itself, TODO could be great if read from the file import

    commands = []
    commands.append(fc.new_simulation())
    commands.append(fc.open(flightstream_file))

    # avoid this part, might be useless
    # rotation_cmds, frame_id = rotation(rotors, aoa["angle"], aoa["axis"])
    # commands = commands[:] + rotation_cmds

    # angle = 0
    # surface = 0  # 0 for all, great for one rotor

    # specific part for one rotor with two blade
    commands = commands[:] + pitch_rotation(rotors)

    TRANSLATION = True
    if TRANSLATION:
        h_space -= fs_p.H_SPACE
        for i, rotor in enumerate(rotors):
            if i == 1:
                for surface in rotor.surfaces:
                    commands.append(fc.surface_translate(
                        h_space, 0, 0, surface_id=surface.index))

    commands.append(set_free_stream(steady, rotors[0]))

    commands.append(set_solver(steady, set_solver_dict))

    ref_velocity = get_ref_velocity(rpm, d=ref_length)

    commands.append(fc.solver_settings(aoa["angle"], beta, freestream_velocity, iterations,
                    ref_velocity, ref_area, ref_length, wake_size, mini_cp, processors, processors))

    if not UNITE_BROKEN:  # TODO Repair this (fsm file)
        commands = commands[:] + \
            [rotor.geometry_unite(dirname) for rotor in rotors]

    if not steady:
        commands = commands[:] + \
            [rotor.create_motion(frame_id=1) for rotor in rotors]

    if steady:
        commands.append(initialize_solver(rotors, periodic))
    else:
        commands.append(initialize_solver(rotors))

    commands.append(fc.solver_analysis_options())  # default value
    commands.append(fc.set_loads_and_moments_units("NEWTONS"))
    # TODO Set moment in N.m not N.ft

    commands.append(fc.start_solver())
    commands.append(fc.export_solver_analysis_spreadsheet(output_file))

    # Specific for photo of one rotor
    commands.append(fc.set_scene(utils_command.Scene.SET_SCENE_YZ_POSITIVE))
    commands.append(fc.set_scene_contour(
        utils_command.Scene_contour.SEPARATION_MARKER))
    if img_path != "":
        commands.append(fc.save_scene_as_image(img_path))
    commands.append(fc.close())
    write_script(script_file, commands)


def get_ref_velocity(rpm, d):
    # calculate from rotational speed
    # v = omega r
    # unit: [d]/s
    omega = rpm * 0.10472  # rad/s
    blade_tip_speed = omega * d/2  # [d]/s
    # print("blade tip speed: " + str(blade_tip_speed))
    return blade_tip_speed


def mk_dir(new_dirName):
    try:
        os.mkdir(new_dirName)
        # print("Directory ", new_dirName,  " Created ")
    except FileExistsError:
        pass
        # print("Directory ", new_dirName,  " already exists")


def naca_main(steady, dirname, flightstream_file, js_extremum=[], rpms=[], pitch_angles=[], periodic=0, nb_j_points: int = 5, j_step: float = 0):
    flightstream_file = os.path.join(dirname, flightstream_file)

    ref_area = 3.281  # ft^2
    if js_extremum == []:
        print("auto js extremum")
        js_extremum = [[0.2, 0.9], [0.2, 1.1], [0.2, 1.3],
                       [0.2, 1.6], [0.2, 1.8], [0.2, 2.2], [0.2, 2.7]]
    if rpms == []:
        print("auto rpms")
        rpms = [1200, 1200, 1000, 1000, 800, 900, 800]  # RPM
    if pitch_angles == []:
        print("auto pitch angle")
        pitch_angles = [i for i in range(0, 46-15, 5)]  # °
    d = 10  # ft

    steady_rel_path = ""
    if steady:
        steady_rel_path = "steady"
    else:
        steady_rel_path = "unsteady"
    # Create folders ->
    # (un)steady/
    #            scripts
    #            outputs
    dir_steady_path = os.path.join(dirname, steady_rel_path)
    mk_dir(dir_steady_path)  # ./(un)steady/

    dir_steady_script = os.path.join(steady_rel_path, "scripts")
    dir_steady_output = os.path.join(steady_rel_path, "outputs")

    dir_steady_script_path = os.path.join(dirname, dir_steady_script)
    dir_steady_output_path = os.path.join(dirname, dir_steady_output)
    mk_dir(dir_steady_script_path)  # ./(un)steady/scripts/
    mk_dir(dir_steady_output_path)  # ./(un)steady/output/

    list_scripts_file = os.path.join(
        dir_steady_script_path, "list_scripts.txt")

    # TODO remove these rotors and create it before
    # rotors= [Rotor(surfaces = [Surface(1, 0), Surface(2, -0)], pitch_axis="Y", rpm=0, rotation_axis = "X")]
    rotors = [utils.Rotor(surfaces=[utils.Surface(1, 0), utils.Surface(2, 0)], pitch_axis="Y",
                          rpm=0, rotation_axis="X")]

    # Writing part
    with open(list_scripts_file, "w+") as f:

        for i in range(len(pitch_angles)):

            # Create angle folders
            dir_angle = "angle_" + \
                str(pitch_angles[i]+fs_p.BASE_PITCH_ANGLE)  # TODO Change this to have the name correct but to have pitch_angle the real pitch angle and not the rotation
            dir_steady_script_angle = os.path.join(
                dir_steady_script, dir_angle)
            dir_steady_output_angle = os.path.join(
                dir_steady_output, dir_angle)

            dir_steady_script_angle_path = os.path.join(
                dirname, dir_steady_script_angle)
            dir_steady_output_angle_path = os.path.join(
                dirname, dir_steady_output_angle)

            # ./(un)steady/scripts/angleXX/
            mk_dir(dir_steady_script_angle_path)
            # ./(un)steady/output/angleXX/
            mk_dir(dir_steady_output_angle_path)

            # conditions
            j_min, j_max = js_extremum[i][0], js_extremum[i][1]

            js = []
            if j_step > 0:
                js = np.arange(j_min, j_max+j_step/2, j_step)
            else:
                js = np.linspace(j_min, j_max, nb_j_points)

            rpm = rpms[i]
            aoa = 0

            # for unsteady:
            angle_dt = 10  # °
            # for angle_dt° rotation
            dt = 1/(np.rad2deg(rpm*RPM2RAD)) * angle_dt
            t_rev = 720/angle_dt * dt  # time to two whole revolutions:

            # final update for rotors
            for rotor in rotors:
                rotor.update_surface_angle(pitch_angles[i])
                rotor.rpm = rpm

            for j in js:
                free_stream_velocity = j * (rpm/60) * d
                # dt # for 6° rotation
                # t_rev = 720/6 * dt # time to two whole revolutions:

                filename_exp = "j_" + f'{j:1.2f}' + ".txt"
                img_exp = "j_" + f'{j:1.2f}' + ".bmp"

                script_file = os.path.join(
                    dir_steady_script_angle, filename_exp)
                # print("script file : " + script_file)
                output_rel_file = os.path.join(
                    dir_steady_output_angle, filename_exp)

                img_rel_path = os.path.join(dir_steady_output_angle, img_exp)
                # print("img_rel_path:")
                # print(img_rel_path)

                output_file = os.path.join(dirname, output_rel_file)
                img_path = os.path.join(dirname, img_rel_path)

                # no rotation
                aoa = {"angle": 0, "axis": "Y"}

                if steady:
                    steady_script(dirname, script_file, flightstream_file, output_file,
                                  img_path, rotors, free_stream_velocity, periodic, d, aoa, ref_area=ref_area)  # rpm, aoa
                else:
                    unsteady_script(dirname, script_file, flightstream_file, output_file,
                                    img_path, rotors, free_stream_velocity, d, angle_dt, ref_area=ref_area)  # rpm, aoa, dt
                f.write(os.path.join(dir_steady_script_angle, filename_exp) + "\n")
    return list_scripts_file


def flugauto_text_matrix_main(steady, dirname, flightstream_file, sets, rotors, d, ref_area, periodic=0, coax=False, pitch=False, spacing=False):
    flightstream_file = os.path.join(dirname, flightstream_file)
    steady_rel_path = ""
    if steady:
        steady_rel_path = "steady"
    else:
        steady_rel_path = "unsteady"
    # Create folders ->
    # (un)steady/
    #            scripts
    #            outputs
    dir_steady_path = os.path.join(dirname, steady_rel_path)
    mk_dir(dir_steady_path)  # ./(un)steady/

    dir_steady_script = os.path.join(steady_rel_path, "scripts")
    dir_steady_output = os.path.join(steady_rel_path, "outputs")

    dir_steady_script_path = os.path.join(dirname, dir_steady_script)
    dir_steady_output_path = os.path.join(dirname, dir_steady_output)
    mk_dir(dir_steady_script_path)  # ./(un)steady/scripts/
    mk_dir(dir_steady_output_path)  # ./(un)steady/output/

    for one_set in sets:

        # sets = [[aoa_angle, freestream_velocity, a_rpm, b_rpm]...]
        aoa_angle = one_set[0]
        freestream_velocity = one_set[1]
        a_rpm = one_set[2]
        if coax:
            b_rpm = one_set[3]

        aoa = {"angle": aoa_angle, "axis": "Y"}
        # TODO final update for rotors
        # for rotor in rotors:
        #     # TODO VERIFY pitch angle rotation ok
        #     # rotor.update_surface_angle(pitch_angles[i])
        #     rotor.rpm = a_rpm

        rotors[0].rpm = a_rpm
        if coax:
            rotors[1].rpm = b_rpm
        # if pitch:
        #     a_pitch = one_set[4]
        #     if coax:
        #         b_pitch = one_set[5]
        #     rotors[0].update_surface_angle(a_pitch)
        #     rotors[1].update_surface_angle(b_pitch)

        # if spacing:
        #     h_space = one_set[5]

        # WARNING the filename SHOULD have the "prop_a_NUMBER_prop_b_NUMBER.txt" format for now (rpm is not stored in the flightstream outputs)
        filename = "angle_" + f'{aoa_angle:3.1f}' + "_freestream_" + \
            f"{freestream_velocity:3.3f}"

        filename = filename + "_prop_a_" + "{:05.0f}".format(a_rpm)
        if coax:
            filename = filename + "_prop_b_" + "{:05.0f}".format(b_rpm)

        # if pitch:
        #     filename = filename + "_pitch_a_" + "{:01.3f}".format(a_pitch)
        #     if coax:
        #         filename = filename + "_pitch_b_" + "{:01.3f}".format(b_pitch)
        # if spacing:
        #     filename = filename + "_periodic_spacing_" + "{:0.4}f".format(h_space)

        # if not steady:
        #     filename += "_prop_b_" + f"{b_rpm:5.0f}"
        filename_exp = filename + ".txt"
        img_exp = filename + ".bmp"

        script_file = os.path.join(
            dir_steady_script, filename_exp)
        output_rel_file = os.path.join(
            dir_steady_output, filename_exp)
        img_rel_path = os.path.join(dir_steady_output, img_exp)

        output_file = os.path.join(dirname, output_rel_file)
        img_path = os.path.join(dirname, img_rel_path)

        # TODO add spacing parameters

        if steady:
            steady_script(dirname, script_file, flightstream_file, output_file,
                          img_path, rotors, freestream_velocity, periodic, d, aoa, ref_area)  # rpm, aoa
            # TODO effective pitch_angle of rotation (Pitch - Base_P_a)
            # free_stream_velocity needed
        else:
            # for unsteady:
            angle_dt = 10  # °
            # for angle_dt° rotation
            # dt = 1/(np.rad2deg(max(a_rpm, b_rpm)*RPM2RAD)) * angle_dt
            # t_rev = 720/angle_dt * dt  # time to two whole revolutions:
            unsteady_script(dirname, script_file, flightstream_file, output_file,
                            img_path, rotors, freestream_velocity, d, angle_dt, aoa, ref_area)  # rpm, aoa, dt
        # f.write(os.path.join(dir_steady_script_angle, filename_exp) + "\n")
        # return list_scripts_file


def creating_folder_and_file(dirname, steady, filename, already_calculated=False):
    steady_rel_path = ""
    if steady:
        steady_rel_path = "steady"
    else:
        steady_rel_path = "unsteady"
    # Create folders ->
    # (un)steady/
    #            scripts
    #            outputs
    dir_steady_path = os.path.join(dirname, steady_rel_path)

    dir_steady_script = os.path.join(steady_rel_path, "scripts")
    dir_steady_output = os.path.join(steady_rel_path, "outputs")

    dir_steady_script_path = os.path.join(dirname, dir_steady_script)
    dir_steady_output_path = os.path.join(dirname, dir_steady_output)

    if not already_calculated:  # path already existing if it has been calculated
        mk_dir(dir_steady_path)  # ./(un)steady/
        mk_dir(dir_steady_script_path)  # ./(un)steady/scripts/
        mk_dir(dir_steady_output_path)  # ./(un)steady/output/

    filename_exp = filename + ".txt"
    img_exp = filename + ".bmp"

    script_file = os.path.join(
        dir_steady_script, filename_exp)
    output_rel_file = os.path.join(
        dir_steady_output, filename_exp)
    img_rel_path = os.path.join(dir_steady_output, img_exp)

    output_file = os.path.join(dirname, output_rel_file)
    img_path = os.path.join(dirname, img_rel_path)

    return script_file, output_file, img_path


def set_conditions(rotors, one_set, coax, pitch, spacing):
    aoa_angle = 0
    freestream_velocity = 0.001  # one_set[1]
    a_rpm = one_set[0]
    if coax:
        b_rpm = one_set[1]

    aoa = {"angle": aoa_angle, "axis": "Y"}

    rotors[0].rpm = a_rpm
    if coax:
        rotors[1].rpm = b_rpm
    if pitch:
        a_pitch = one_set[2]
        if coax:
            b_pitch = one_set[3]
        rotors[0].update_surface_angle(a_pitch)
        rotors[1].update_surface_angle(b_pitch)

    if spacing:
        h_space = one_set[4]

    filename = "angle_" + f'{aoa_angle:3.1f}' + "_freestream_" + \
        f"{freestream_velocity:3.3f}"

    filename = filename + "_prop_a_" + "{:05.0f}".format(a_rpm)
    if coax:
        filename = filename + "_prop_b_" + "{:05.0f}".format(b_rpm)

    if pitch:
        filename = filename + "_pitch_a_" + "{:01.3f}".format(a_pitch)
        if coax:
            filename = filename + "_pitch_b_" + "{:01.3f}".format(b_pitch)
    if spacing:
        filename = filename + "_periodic_spacing_" + "{:0.4f}".format(h_space)
    return filename, h_space, freestream_velocity, aoa


def optimization(steady, dirname, flightstream_file, one_set, rotors, d, ref_area, periodic=0, coax=fs_p.COAX, pitch=fs_p.PITCH, spacing=fs_p.SPACING, already_calculated=False):

    filename, h_space, freestream_velocity, aoa = set_conditions(
        rotors, one_set, coax, pitch, spacing)

    script_file, output_file, img_path = creating_folder_and_file(
        dirname, steady, filename)

    flightstream_file = os.path.join(dirname, flightstream_file)

    if not already_calculated:
        if steady:
            steady_script(dirname, script_file, flightstream_file, output_file,
                          img_path, rotors, freestream_velocity, periodic, d, aoa, ref_area)  # rpm, aoa
        else:
            angle_dt = 10  # °
            unsteady_script(dirname, script_file, flightstream_file, output_file,
                            img_path, rotors, freestream_velocity, d, angle_dt, aoa, ref_area, h_space=h_space)  # rpm, aoa, dt
        # f.write(os.path.join(dir_steady_script_angle, filename_exp) + "\n")
    return script_file, output_file


if __name__ == "__main__":
    steady = True
    dirname = os.path.dirname(__file__)
    flightstream_file = "flightstream_files/Test_unsteady_solver_-_no_motion.fsm"
    naca_main(steady, dirname, flightstream_file, js_extremum=[
        [0.2, 1.1]], rpms=[1200], pitch_angles=[5], nb_j_points=5)
