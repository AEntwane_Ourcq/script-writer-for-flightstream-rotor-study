import fnmatch
import os
from operator import itemgetter

import matplotlib.pyplot as plt
import numpy as np

from fs_code import utils, flightstream_project as fs_p

ECART = 0  # TODO change  # in 0 to 45 deg
MIN_ANGLE = ECART
MAX_ANGLE = 90-ECART


def recursive_path_join(lists):
    print(lists)
    if len(lists) == 1:
        return lists[0]
    elif len(lists) > 1:
        lists[-2] = os.path.join(lists[-2], lists[-1])
        return recursive_path_join(lists[:-1])
    else:
        raise Exception


def flug_one_rotor(csv_file, single=True):
    file = csv_file
    coaxfile = csv_file
    if single:
        file_path = recursive_path_join(
            [os.path.dirname(__file__), "..", "flugauto_files", dir, file])
        rpms, thrusts_kgf, torques_metric, powers = single_rotor(file_path)
        print(rpms, thrusts_kgf, torques_metric)
        plt.figure()
        plt.plot(rpms, thrusts_kgf, "b+")
        plt.figure()
        plt.plot(rpms, powers, "r+")
        plt.show()

    else:
        file_path = recursive_path_join(
            [os.path.dirname(__file__), "..", "flugauto_files", dir, coaxfile])
        upper, lower = coax_rotor(file_path)


def single_rotor(filepath):
    rpms = []
    thrusts_kgf = []
    torques_metric = []
    powers = []
    with open(filepath, "r") as f:
        index_lists = {"thrust": 0, "torque": 0, "rpm": 0, "power": 0}
        first_line = True
        for line in f:
            if first_line:
                grandeurs = line.split(",")
                for i, g in enumerate(grandeurs):
                    if ("Thrust (kgf)" in g):
                        index_lists["thrust"] = i
                        print(g)
                    elif "Torque" in g:
                        index_lists["torque"] = i
                        print(g)
                    elif "Motor Optical Speed (RPM)" in g:
                        index_lists["rpm"] = i
                        print(g)
                    elif "Electrical Power (W)" in g:
                        index_lists["power"] = i
                        print(g)
                first_line = False
            else:
                values = line.split(",")
                rpms.append(float(values[index_lists["rpm"]]))
                thrusts_kgf.append(float(values[index_lists["thrust"]]))
                torques_metric.append(float(values[index_lists["torque"]]))
                powers.append(float(values[index_lists["power"]]))
    print(grandeurs[index_lists["thrust"]])
    print(grandeurs[index_lists["torque"]])
    print(grandeurs[index_lists["rpm"]])
    print(grandeurs[index_lists["power"]])
    return rpms, thrusts_kgf, torques_metric, powers


def coax_rotor(filepath, ccws=[True, False]):
    rpms = []
    thrusts_kgf = []
    torques = []
    powers = []

    rpms_upper = []
    thrusts_kgf_upper = []
    torques_upper = []
    powers_upper = []
    rpms_lower = []
    thrusts_kgf_lower = []
    torques_lower = []
    powers_lower = []
    with open(filepath, "r") as f:
        index_lists = {"thrust_upper": 0, "torque_upper": 0, "rpm_upper": 0, "power_upper": 0,
                       "thrust_lower": 0, "torque_lower": 0, "rpm_lower": 0, "power_lower": 0}
        first_line = True
        for line in f:
            if first_line:
                grandeurs = line.split(",")
                print("first test : ")
                print("Torque A (N·m)" in grandeurs[0])

                print("grandeur : ", grandeurs)
                # TODO resolve encoding NÂ·m -> N·m
                for i, g in enumerate(grandeurs):
                    # print(g, i)
                    if "Thrust A (kgf)" in g:
                        index_lists["thrust_upper"] = i
                        print(g)
                    # "Torque A (NÂ·m)" for previous file of flugauto
                    elif "Torque A (N·m)" in g or "Torque A (NÂ·m)" in g:
                        index_lists["torque_upper"] = i
                        print(g)
                    elif "Motor Optical Speed A (RPM)" in g:
                        index_lists["rpm_upper"] = i
                        print(g)
                    elif "Electrical Power A (W)" in g:
                        index_lists["power_upper"] = i
                        print(g)
                    if "Thrust B (kgf)" in g:
                        index_lists["thrust_lower"] = i
                        print(g)
                    elif "Torque B (N·m)" in g or "Torque B (NÂ·m)" in g:
                        index_lists["torque_lower"] = i
                        print(g)
                    elif "Motor Optical Speed B (RPM)" in g:
                        index_lists["rpm_lower"] = i
                        print(g)
                    elif "Electrical Power B (W)" in g:
                        index_lists["power_lower"] = i
                        print(g)
                first_line = False
                print("first line of ref: ")
                print(grandeurs)
                print(index_lists)
            else:
                values = line.split(",")
                rpms_upper.append(float(values[index_lists["rpm_upper"]]))
                thrusts_kgf_upper.append(
                    float(values[index_lists["thrust_upper"]]))
                torques_upper.append(
                    float(values[index_lists["torque_upper"]]))
                powers_upper.append(float(values[index_lists["power_upper"]]))
                rpms_lower.append(float(values[index_lists["rpm_lower"]]))
                thrusts_kgf_lower.append(
                    float(values[index_lists["thrust_lower"]]))
                torques_lower.append(
                    float(values[index_lists["torque_lower"]]))
                powers_lower.append(float(values[index_lists["power_lower"]]))
    print(grandeurs[index_lists["thrust_upper"]])
    print(grandeurs[index_lists["thrust_lower"]])
    print(grandeurs[index_lists["torque_upper"]])
    print(grandeurs[index_lists["torque_lower"]])
    print(grandeurs[index_lists["rpm_upper"]])
    print(grandeurs[index_lists["rpm_lower"]])
    print(grandeurs[index_lists["power_upper"]])
    print(grandeurs[index_lists["power_lower"]])

    upper = utils.Flug_verif(rpms=rpms_upper, thrusts_kgf=thrusts_kgf_upper,
                             powers=powers_upper, torques=torques_upper, ccw=ccws[0])
    lower = utils.Flug_verif(rpms=[rpm for rpm in rpms_lower],
                             thrusts_kgf=thrusts_kgf_lower, powers=powers_lower, torques=torques_lower, ccw=ccws[1])
    return upper, lower


def export_data(file_out, files):
    """Read all data useful and store it in one unique file"""
    uppers, lowers = [], []
    for file in files:
        filepath = file
        upper, lower = coax_rotor(filepath)
        uppers.append(upper)
        lowers.append(lower)
    with open(file_out, "w") as f:
        f.write(
            "Motor Optical Speed A (RPM),Thrust A (kgf),Torque A (N·m)" + "," +
            "Motor Optical Speed B (RPM),Thrust B (kgf),Torque B (N·m)\n"
        )
        for i in range(len(uppers)):
            upper = uppers[i]
            lower = lowers[i]
            for j in range(len(upper.rpms)):
                f.write(
                    f"{upper.rpms[j]:5.0f}" + "," +
                    f"{upper.thrusts_kgf[j]:5.5f}" + "," +
                    f"{upper.torques[j]:5.5f}" + "," +
                    f"{lower.rpms[j]:5.0f}" + "," +
                    f"{lower.thrusts_kgf[j]:5.5f}" + "," +
                    f"{lower.torques[j]:5.5f}" + "\n"
                )


def csv_ls(dirname="."):
    outputs = []
    list_of_files = os.listdir(dirname)
    txt_pattern = "*.csv"
    for file in list_of_files:
        if fnmatch.fnmatch(file, txt_pattern):
            outputs.append(os.path.join(dirname, file))
    return outputs


def fusion_data(dirname, fusion_file, list_to_folder):
    rel_path_folder = recursive_path_join(
        [dirname] + list_to_folder)
    files = []
    for rel_folder in ["A static sweep B", "A sweep static B"]:
        folder = os.path.join(rel_path_folder, rel_folder)
        files = files + csv_ls(folder)
    export_data(fusion_file, files)


def purge_data(dirname, fusion_file):
    # remove the points if too close from an actual one
    purge_file = fusion_file[:-4] + "_purged" + ".csv"
    first_line = True
    rpm_sep = 500*3  # to define (circle)
    a_b_rpms = []
    print("purging data")
    with open(purge_file, "w") as f_out:

        with open(fusion_file, "r") as f_in:
            i = 0
            k = 0
            for line in f_in:
                line_l = line.split(",")

                if first_line:
                    index_a_rpm = line_l.index("Motor Optical Speed A (RPM)")
                    index_b_rpm = line_l.index("Motor Optical Speed B (RPM)")
                    f_out.write(line)
                    first_line = False

                else:
                    k += 1
                    a_rpm = float(line_l[index_a_rpm])
                    b_rpm = float(line_l[index_b_rpm])

                    if len(a_b_rpms) == 0 and a_rpm > 0 and b_rpm > 0:
                        print("should be only one time")
                        a_b_rpms.append((a_rpm, b_rpm))
                        f_out.write(line)
                    else:
                        to_add = True
                        angle = np.rad2deg(np.arctan2(b_rpm, a_rpm))

                        if (angle < MIN_ANGLE or angle > MAX_ANGLE):
                            to_add = False

                        else:
                            for (a, b) in a_b_rpms:
                                dist = np.sqrt(((a-a_rpm)**2 + (b - b_rpm)**2))
                                if dist < rpm_sep:
                                    to_add = False
                                    break
                        if to_add:
                            i += 1
                            print(i, len(a_b_rpms))
                            a_b_rpms.append((a_rpm, b_rpm))
                            f_out.write(line)


def plot(fusion_file):
    purge_file = fusion_file[:-4] + "_purged" + ".csv"

    def reading(file):
        a_rpms = []
        b_rpms = []
        first_line = True
        with open(file, "r") as f:
            for line in f:
                line_l = line.split(",")
                # print("line: ", line)
                if first_line:
                    index_a_rpm = line_l.index("Motor Optical Speed A (RPM)")
                    index_b_rpm = line_l.index("Motor Optical Speed B (RPM)")
                    # f_out.write(line)
                    first_line = False
                else:
                    a_rpm = float(line_l[index_a_rpm])
                    b_rpm = float(line_l[index_b_rpm])
                    a_rpms.append(a_rpm)
                    b_rpms.append(b_rpm)
        return a_rpms, b_rpms

    a_all, b_all = reading(fusion_file)
    a_p, b_p = reading(purge_file)
    print("points kept: ", len(a_p))
    plt.plot(a_all, b_all, ".")
    plt.plot(a_p, b_p, 'x')
    xs = np.linspace(0, fs_p.ABS_MAX_RPM, 100)
    ymin = []
    ymax = []
    for x in xs:
        y_max = x/np.tan(np.deg2rad(90-MAX_ANGLE))
        y_min = x/np.tan(np.deg2rad(90-MIN_ANGLE))
        print("both y", y_min, y_max)
        if y_max < fs_p.ABS_MAX_RPM:
            ymax.append(y_max)
        if y_min < fs_p.ABS_MAX_RPM:
            ymin.append(y_min)
    print("Y0")
    print((ymin))
    print("Y1")
    print((ymax))

    xmax = xs[:len(ymax)]
    xmin = xs[:len(ymin)]
    print(len(xmin))
    print(len(xmax))
    print(len(ymin))
    print(len(ymax))
    plt.plot(xmin, ymin, '--')
    plt.plot(xmax, ymax, '--')
    plt.legend(["all", "keeping", "min_angle", "max_angle"])
    plt.show()


if __name__ == "__main__":
    # csv_file =
    # flug_one_rotor(csv_file, single = False)
    dirname = os.path.join(os.path.dirname(__file__), "..")
    fusion_file = recursive_path_join([dirname, "merging_all_data.csv"])
    fusion_data(dirname, fusion_file)
    purge_data(dirname, fusion_file)
