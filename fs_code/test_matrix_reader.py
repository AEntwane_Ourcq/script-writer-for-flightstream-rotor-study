import itertools
import os

import numpy as np
import pandas as pd

from fs_code import flightstream_project as fs_p

DATA_FOLDER = ""  # to set #TODO remove the need of absolute path


def read_xlsx(single_rotor):
    file_rel_path = './test_matrix.xlsx'
    file_path = os.path.join(DATA_FOLDER, file_rel_path)
    # f = pd.read_excel(file_path)
    xls = pd.ExcelFile(file_path)

    if single_rotor:
        df1 = pd.read_excel(xls, 'SINGLE')
        nf = df1.to_numpy()
        cases = nf[1:, 7:]
        rt_cases = cases[:, 0:3]  # pitch angle, freestream, propeller A rpm
        constant_parameters = nf[:, :5]
        d = {}
        for i in range(3):
            d[str(constant_parameters[i, 0]) + "_" +
              str(constant_parameters[i, 1])] = constant_parameters[i, 2]
        for i in range(5, 8):
            d[str(constant_parameters[i, 0]) + "_" + str(constant_parameters[i, 1])] = [
                constant_parameters[i, 2], constant_parameters[i, 3], constant_parameters[i, 4]]

        print(d)
        return d

    else:
        df2 = pd.read_excel(xls, 'COAX')
        nf = df2.to_numpy()
        cases = nf[1:, 7:]
        # pitch angle, freestream, propeller A rpm, propeller B rpm
        rt_cases = np.hstack((cases[:, 0:3], np.array([cases[:, 6]]).T))
        return rt_cases


def main(single_rotor=True):
    dictionnary = read_xlsx(single_rotor)
    keys = dictionnary.keys()
    for key in keys:
        if "Angle" in key:
            key_angle = key
        elif "Freestream" in key:
            key_freestream = key
        elif "Propeller" in key and "A" in key:
            key_prop_a = key

    def linspacing(key):
        list_tuple = dictionnary.get(key)
        return np.linspace(list_tuple[0], list_tuple[1], list_tuple[2])
    incident_angles = linspacing(key_angle)
    freestream_velocities = linspacing(key_freestream)
    propeller_a_rpm = linspacing(key_prop_a)

    cartesian_product = [e for e in itertools.product(
        incident_angles, freestream_velocities, propeller_a_rpm)]

    # TODO read d
    d = fs_p.DIAMETER
    # cartesian_product = [[, , ], [, , ]]
    return cartesian_product, d


def test_matrix_rpm_only(rpms, freestream_velocity=0):
    incident_angles = [0]
    freestream_velocities = [freestream_velocity]
    return [e for e in itertools.product(incident_angles, freestream_velocities, rpms)]


def test_matrix_coax_rpm_only(rpms_a, rpms_b, freestream_velocity=0):
    incident_angles = [0]
    freestream_velocities = [freestream_velocity]
    return [e for e in itertools.product(incident_angles, freestream_velocities, rpms_a, rpms_b)]


if __name__ == "__main__":
    single_rotor = True
    dictionnary = read_xlsx(single_rotor)

    # for case in cases:
    #     print("Incident Angle : ", case[0])
    #     print("Freestream Velocity : ", case[1])
    #     print("Propeller A RPM : ", case[2])
    #     if not single_rotor:
    #         print("Propeller B RPM : ", case[3])
    keys = dictionnary.keys()
    for key in keys:
        if "Angle" in key:
            key_angle = key
        elif "Freestream" in key:
            key_freestream = key
        elif "Propeller" in key and "A" in key:
            key_prop_a = key

    def linspacing(key):
        list_tuple = dictionnary.get(key)
        return np.linspace(list_tuple[0], list_tuple[1], list_tuple[2])
    incident_angles = linspacing(key_angle)
    freestream_velocities = linspacing(key_freestream)
    propeller_a_rpm = linspacing(key_prop_a)
    print("freestream_velocities : ", freestream_velocities)
    print("angles : ", incident_angles)
    print("propeller_a_rpm : ", propeller_a_rpm)

    cartesian_product = [e for e in itertools.product(
        incident_angles, freestream_velocities, propeller_a_rpm)]
    print(cartesian_product)
