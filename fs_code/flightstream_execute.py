import os
import sys

FLIGHTSTREAM_RELATIVE_PATH = os.path.join(
    "flightstream_files", "FlightStream.lnk")


def test(script):
    FLIGHTSTREAM_PATH = os.path.join("flightstream_files", "FlightStream.lnk")
    CMD = FLIGHTSTREAM_PATH + " -script " + script
    os.system(CMD)


def main_list_file(dirname, list_scripts_file, hidden=False):
    list_scripts = []
    with open(list_scripts_file, 'r') as f:
        for script_file in f:
            list_scripts.append(script_file)
    main_list(dirname, list_scripts, hidden)


def main_list(dirname, list_scripts, hidden=False):
    # hidden if no interactions or images saving needed
    flightstream_path = os.path.join(dirname, FLIGHTSTREAM_RELATIVE_PATH)
    hidden_str = ""
    if hidden:
        hidden_str += " -hidden "
    # n = len(list_scripts)
    # i = 1
    for script_file in list_scripts:
        # print("progression: " + str(i) + "/" + str(n))
        # i += 1
        script_file_path = os.path.join(dirname, script_file)
        fs_cmd = flightstream_path + hidden_str + " -script " + script_file_path
        os.system(fs_cmd)
