import re

from numpy import number, pi

# filename_test = dirname + "./outputs/first_data_output.txt"

def filename_info_parser(name):
    name = name.split("\\")[-1]  # Windows path

    # name = "angle_XXXX_freestream_XXXX_prop_a_XXXX_prop_b_XXXX_pitch_a_XXXX_pitch_b_XXXX_periodic_spacing_XXXX.txt"
    info_dict = {}
    number_rule = '\d+[.]?\d*'
    number_regexp = re.compile(number_rule)
    sign_number_regexp = re.compile("-?"+number_rule)
    rpm_prop_regexp = re.compile('prop_[ab]_' + number_rule)
    pitch_regexp = re.compile("pitch_[ab]_" + "-?" + number_rule)
    spacing_regexp = re.compile("spacing_" + number_rule)
    # TODO angle and freestream if needed..
    rpms = rpm_prop_regexp.findall(name)
    pitchs = pitch_regexp.findall(name)

    def type_regexp_result(val, type_expected):
        # print("val: ", val)
        f = sign_number_regexp.findall(val)
        # print("f: ", f)
        if len(f) == 1:
            f_typed = type_expected(f[0])
            return f_typed
        else:
            print("too many values gathered:")
            print(val)
            print("in:")
            print(val)
            raise Exception

    def splitting_a_b(values, type_expected):
        a_val = None
        b_val = None
        for val in values:
            f_typed = type_regexp_result(val, type_expected)
            if "_a_" in val:
                a_val = f_typed
            elif "_b_" in val:
                b_val = f_typed
            else:
                print("unexpected splitting (no _a_ or _b_) in :")
                print(val)
                raise Exception
        return a_val, b_val

    a_rpm, b_rpm = splitting_a_b(rpms, int)
    a_pitch, b_pitch = splitting_a_b(pitchs, float)

    info_dict['a_rpm'] = a_rpm
    info_dict['b_rpm'] = b_rpm
    info_dict["a_pitch"] = a_pitch
    info_dict["b_pitch"] = b_pitch
    info_dict["spacing"] = type_regexp_result(
        spacing_regexp.findall(name)[0], float)

    # print(info_dict)
    return info_dict


def generic_parse_file(filename, angle):
    """WARNING depends on the name file to have the correct rpm (that is not stored in the file itself...)"""
    out_line = ""
    l = Line()
    l.angle = angle
    with open(filename, "r") as f:
        c = 0
        elements = []
        element_counter = 0
        for line in f:
            newline = line.strip()
            # print(newline)
            if newline.startswith("Angle of attack"):
                AoA = float(newline.split()[-1])
                l.aoa = AoA
            elif newline.startswith("Side-slip angle"):
                l.beta = float(newline.split()[-1])
            elif newline.startswith("Free-stream velocity"):
                l.freestream_velocity = float(newline.split()[-1])
            elif newline.startswith("Solver mode"):
                l.solver_mode = newline.strip("Solver mode:")
            elif newline.startswith("Reference velocity"):
                l.ref_velocity = float(newline.split()[-1])
            elif newline.startswith("Reference length"):
                l.ref_length = float(newline.split()[-1])
                # = newline.split()[-2]
                # print("ref_length line: ", newline.split()[-2])
            elif newline.startswith("Reference area"):
                l.ref_area = float(newline.split()[-1])
            elif newline.startswith("Force Units"):
                l.force_unit = newline.split()[-1]
            elif newline.startswith("Moment Units"):
                l.moment_unit = newline.split()[-1]
            elif newline.startswith("Freestream Rotating Reference Frame RPM"):
                l.rpm = float(newline.split()[-1])

            elif newline.endswith(","):
                line_splitted = newline.split("\x00\x00,")
                elements.append(Element(line_splitted))

    return l, elements, filename_info_parser(filename)


def parse_file_simple_rotor(filename, angle=0):
    l, elements, info_dict = generic_parse_file(filename, angle)
    # rpm_a = info_dict["a_rpm"]
    for el in elements:
        if "Total" in el.name:
            l.add_forces(el)
    return l


def parse_file_coax_rotor(filename, expected_elements, angle=0):
    total, elements, info_dict = generic_parse_file(filename, angle)
    for el in elements:
        if "Total" in el.name:
            total.add_forces(el)
    upper_result = Element(["upper"], info_dict["a_rpm"])
    lower_result = Element(["lower"], info_dict["b_rpm"])
    [upper_result.sum(e) for e in elements if e.name in expected_elements[0]]
    [lower_result.sum(e) for e in elements if e.name in expected_elements[1]]
    return total, upper_result, lower_result, info_dict


class Line():
    def __init__(self):
        self.angle = 0  # deg
        self.aoa = 0
        self.beta = 0
        self.freestream_velocity = 0
        self.solver_mode = ""
        self.ref_velocity = 0
        self.ref_length = 0
        self.ref_area = 0
        self.force_unit = ""
        self.moment_unit = ""
        self.rpm = 0
        self.name = ""
        self.fx = 0
        self.fy = 0
        self.fz = 0
        self.lift = 0
        self.di = 0
        self.do = 0
        self.mx = 0
        self.my = 0
        self.mz = 0
        # calculate value
        self.j = 0
        self.ct = 0
        self.cp = 0
        self.efficiency = 0
        self.power = 0
        self.thrust = 0

    def update(self):
        # conversion to metric unit
        if self.moment_unit.find("Newton-Feet") != -1:
            self.mx *= 0.3048
            self.my *= 0.3048
            self.mz *= 0.3048

        if self.rpm == 0:
            self.rpm = self.ref_velocity / (self.ref_length/2) / 0.10472

        rps = self.rpm/60
        self.j = self.freestream_velocity/(rps * self.ref_length)
        rho = 1.225
        ref_length_meter = self.ref_length * 0.3048

        self.power = self.mx * rps * 2 * pi
        self.thrust = -self.fx
        self.ct = - self.fx / (rho * rps**2 * (ref_length_meter)**4)
        self.cp = self.power/(rho * rps**3 * ref_length_meter**5)

        if self.ct > 0 and self.cp != 0:
            self.efficiency = self.j * self.ct/self.cp

    def add_forces(self, el):
        self.name = el.name
        self.fx = el.fx
        self.fy = el.fy
        self.fz = el.fz
        self.lift = el.lift
        self.di = el.di
        self.do = el.do
        self.mx = el.mx
        self.my = el.my
        self.mz = el.mz
        self.update()

    def __repr__(self):
        return f'{self.j:1.2f}' + ", " + f'{self.ct:1.5f}' + ", " + f'{self.cp:1.5f}' + ", " + f'{self.efficiency:1.5f}' + ", " + f'{self.angle:2.0f}'

    def thrust_power(self):
        return f'{self.thrust:1.5f}' + ", " + f'{self.mx:1.5f}' + ", " + f'{self.power:1.5f}'


class Element():
    def __init__(self, args, rpm=0):
        if len(args) == 11:
            self.name = args[0]
            self.fx = float(args[1])
            self.fy = float(args[2])
            self.fz = float(args[3])
            self.lift = float(args[4])
            self.di = float(args[5])
            self.do = float(args[6])
            self.mx = float(args[7])
            self.my = float(args[8])
            self.mz = float(args[9])

        elif len(args) == 1:
            self.name = args[0]
            self.fx = 0
            self.fy = 0
            self.fz = 0
            self.lift = 0
            self.di = 0
            self.do = 0
            self.mx = 0
            self.my = 0
            self.mz = 0
        else:
            print(args)
            print(len(args))
            print("error in element creation")
        self.rpm = rpm

    def sum(self, other):
        self.fx += other.fx
        self.fy += other.fy
        self.fz += other.fz
        self.lift += other.lift
        self.di += other.di
        self.do += other.do
        self.mx += other.mx
        self.my += other.my
        self.mz += other.mz
