import os

from fs_code import utils_command


def convert(command: str, value) -> str:
    line = command + " "
    if type(value) == bool:
        bool_value = "ENABLE"
        if value == False:
            bool_value = "DISABLE"
        line += bool_value
    elif type(value) == float:
        line += f'{value:.4f}'
    else:
        line += str(value)
    line += "\n"
    return line


def dict_convert(dictionary, parameter: str) -> str:
    return convert(parameter, dictionary.get(parameter))


def concatenate(list) -> str:
    """List of string"""
    s = ""
    for i, elmt in enumerate(list):
        s += elmt
        if i == 0:
            s += "\n"
    return s + "\n"


def new_simulation():
    return "NEW_SIMULATION\n\n"


def open(path: str) -> str:
    return "OPEN\n"+path+"\n\n"


def surface_rotate(frame: int = 1, axis: str = "Y", angle: float = 0, surface: int = 0, split_vertices: bool = True) -> list:
    l = ["SURFACE_ROTATE", convert("FRAME", frame), convert("AXIS", axis), convert(
        "ANGLE", angle), convert("SURFACE", surface), convert("SPLIT_VERTICES", split_vertices)]
    return concatenate(l)


def edit_freestream(type: str, param_dict) -> list:
    """param_dict dictionary of parameters such as {"RPM":1200, "FRAME":1, "AXIS":"X"}"""
    l_add = []
    if type != "CONSTANT":
        if type == "ROTATIONAL":
            l_add = [convert("RPM", param_dict.get("RPM")), convert(
                "FRAME", param_dict.get("FRAME")), convert("AXIS", param_dict.get("AXIS"))]
    l = ["EDIT_FREESTREAM", convert("TYPE", type)]
    return concatenate(l + l_add)


def set_solver(type: str, param) -> list:
    """param a dictionary"""
    l = []
    if type == "ROTARY":
        l = [dict_convert(param, "BLOCK_ITERATIONS"), dict_convert(
            param, "INDUCED_VELOCITY_RESIDUAL")]
    elif type == "UNSTEADY":
        l = [dict_convert(param, "TIME_ITERATIONS"),
             dict_convert(param, "DELTA_TIME"),
             dict_convert(param, "ACTIVE_SCRIPT")]
    return concatenate(["SET_SOLVER", str(type)+"\n"] + l)


def solver_settings(aoa: float = 0, beta: float = 0, freestream_velocity: float = 0, iterations: int = 100, ref_velocity: float = 0, ref_area: float = 10, ref_length: float = 10, wake_size: float = 300, mini_cp: float = -100, processors: int = 4, parallel_cores: int = 4, convergence_limit: float = 2e-5, forced_run: bool = False, compressibility: bool = True, ) -> list:
    return concatenate(["SOLVER_SETTINGS",
                        convert("ANGLE_OF_ATTACK", aoa),
                        convert("SIDESLIP_ANGLE", beta),
                        convert("FREESTREAM_VELOCITY", freestream_velocity),
                        convert("ITERATIONS", iterations),
                        convert("CONVERGENCE_LIMIT", format(
                            convergence_limit, '.5f')),
                        convert("FORCED_RUN", forced_run),
                        convert("COMPRESSIBILITY", compressibility),
                        convert("REFERENCE_VELOCITY", ref_velocity),
                        convert("REFERENCE_AREA", ref_area),
                        convert("REFERENCE_LENGTH", ref_length),
                        convert("PROCESSORS", processors),
                        convert("WAKE_SIZE", wake_size),
                        convert("SOLVER_MINIMUM_CP", mini_cp),
                        convert("SOLVER_PARALLEL_CORES", parallel_cores)])


def initialize_solver(surfaces=2, wake_termination_x="DEFAULT", symmetry_type: str = "NONE", symmetry_periodicity: int = 0, load_frame: int = 1, proximity_avoidance: bool = True, stabilization: bool = True, stabilization_strength: float = 1.0, fast_multipole: bool = False) -> list:
    """
    basic utilization: surfaces:int=2 -> initialize for all surfaces until 2 included
    'precise' utilization:  surfaces:[int]=[1,2,5] -> initialize for all surfaces in the list
    """
    nb_surfaces, surface_lines = initialize_surfaces(surfaces)
    return concatenate(["INITIALIZE_SOLVER",
                        convert("SURFACES", nb_surfaces)] +
                       surface_lines +
                       [convert("WAKE_TERMINATION_X", wake_termination_x),
                        convert("SYMMETRY_TYPE", symmetry_type),
                        convert("SYMMETRY_PERIODICITY", symmetry_periodicity),
                        convert("LOAD_FRAME", load_frame),
                        convert("PROXIMITY_AVOIDANCE", proximity_avoidance),
                        convert("STABILIZATION", stabilization),
                        convert("STABILIZATION_STRENGTH",
                                stabilization_strength),
                        convert("FAST_MULTIPOLE", fast_multipole)])


def initialize_surfaces(surfaces) -> tuple[int, list]:
    """See initialize_solver description"""
    surfaces_type = type(surfaces)
    if surfaces_type == int:
        return surfaces, [str(i+1) + ",0,ENABLE\n" for i in range(surfaces)]
    elif surfaces_type == list:
        return len(surfaces), [str(index) + ",0,ENABLE\n" for index in surfaces]
    else:
        print("Unexpected type: " + str(surfaces_type) +
              " should be int or list of int")
        raise Exception


def solver_analysis_options(load_frame: int = 1, lift_model: str = "VORTICITY", drag_model: str = "VORTICITY", moment_model: str = "LINEAR_PRESSURE", compute_symmetry_loads: bool = True, boundary_layer_type: int = 1, boundary_layer_drag: str = "MOMENTUM_INTEGRAL", flow_separation: bool = True) -> list:
    return concatenate(["SOLVER_ANALYSIS_OPTIONS",
                        convert("LOAD_FRAME", load_frame),
                        convert("LIFT_MODEL", lift_model),
                        convert("DRAG_MODEL", drag_model),
                        convert("MOMENT_MODEL", moment_model),
                        convert("COMPUTE_SYMMETRY_LOADS",
                                compute_symmetry_loads),
                        convert("BOUNDARY_LAYER_TYPE", boundary_layer_type),
                        convert("BOUNDARY_LAYER_DRAG", boundary_layer_drag),
                        convert("FLOW_SEPARATION", flow_separation)])


def set_loads_and_moments_units(unit: str = "NEWTONS") -> list:
    return convert("SET_LOADS_AND_MOMENTS_UNITS", unit)+"\n"


def start_solver() -> list:
    return concatenate(["START_SOLVER"])


def export_solver_analysis_spreadsheet(path) -> list:
    return "EXPORT_SOLVER_ANALYSIS_SPREADSHEET\n" + path + "\n\n"


def close():
    return "CLOSE_FLIGHTSTREAM\n"


def geometry_unite(openvsp_path, bodies=[1, 2]):
    l = []
    l.append("BOOLEAN_UNITE_GEOMETRY")
    l.append("BODIES " + str(len(bodies)) + "\n")
    st = ""
    for i in range(len(bodies)):
        st += str(bodies[i])
        if i < len(bodies)-1:
            st += ","
        else:
            st += "\n"
    l.append(st)
    l.append("OPENVSP_PATH " + openvsp_path + "\n")
    return concatenate(l)


def create_euclidean_motion():
    return "CREATE_NEW_MOTION_EUCLIDEAN\n"


def set_motion_coordinate_system(id=1, frame=1):
    return "SET_MOTION_COORDINATE_SYSTEM " + str(id) + " " + str(frame) + "\n"


def set_motion_start_time(id=1, time=0):
    return "SET_MOTION_START_TIME " + str(id) + " " + str(time) + "\n"


def set_motion_velocity(id=1, x=0, y=0, z=0):
    return "SET_MOTION_VELOCITY " + str(id) + " " + f"{x:.3f}" + " " + f"{y:.3f}" + " " + f"{z:.3f}" + "\n"


def set_motion_acceleration(id=1, x=0, y=0, z=0):
    return "SET_MOTION_ACCELERATION " + str(id) + " " + f"{x:.3f}" + " " + f"{y:.3f}" + " " + f"{z:.3f}" + "\n"


def set_motion_angular_velocity(id=1, x=0, y=0, z=0):
    return "SET_MOTION_ANGULAR_VELOCITY " + str(id) + " " + f"{x:.3f}" + " " + f"{y:.3f}" + " " + f"{z:.3f}" + "\n"


def set_motion_angular_acceleration(id=1, x=0, y=0, z=0):
    return "SET_MOTION_ANGULAR_ACCELERATION " + str(id) + " " + f"{x:.3f}" + " " + f"{y:.3f}" + " " + f"{z:.3f}" + "\n"


def set_motion_unstable_wake_terminate(id=1, enable=True):
    cmd = "SET_MOTION_UNSTABLE_WAKE_TERMINATE " + str(id)
    return convert(cmd, enable)


def set_motion_wake_relaxation(id=1, enable=True):
    cmd = "SET_MOTION_WAKE_RELAXATION " + str(id)
    return convert(cmd, enable)


def set_motion_is_rotor(id=1, enable=True, axis="X"):
    cmd = "SET_MOTION_IS_ROTOR " + str(id) + " "
    if enable:
        cmd += "ENABLE"
    else:
        cmd += "DISABLE"
    return convert(cmd, axis)


def set_motion_boundaries(id=1, surfaces=[1]):
    n = len(surfaces)
    cmd = "SET_MOTION_BOUNDARIES " + str(id) + " " + str(n) + "\n"
    for i in range(n):
        cmd += str(surfaces[i])
        if i <= n-1:
            cmd += ","
    return cmd + "\n"


def create_rotor_motion(id=1, axis="X", surfaces=[1], angular_velocities=[1, 0, 0], angular_accelerations=[1, 0, 0], start_time=0, unstable_wake_terminate=True, wake_relaxation=True):
    cmds = []
    cmds.append(create_euclidean_motion())
    cmds.append(set_motion_start_time(id, start_time))
    cmds.append(set_motion_angular_velocity(
        id, angular_velocities[0], angular_velocities[1], angular_velocities[2]))
    cmds.append(set_motion_angular_acceleration(
        id, angular_accelerations[0], angular_accelerations[1], angular_accelerations[2]))
    cmds.append(set_motion_unstable_wake_terminate(
        id, unstable_wake_terminate))
    cmds.append(set_motion_wake_relaxation(id, wake_relaxation))
    cmds.append(set_motion_is_rotor(id, True, axis))
    cmds.append(set_motion_boundaries(id, surfaces))
    return concatenate(cmds)


def set_scene_contour(scene_contour: utils_command.Scene_contour):
    # SET_SCENE_CONTOUR
    # VARIABLE 4
    return concatenate(["SET_SCENE_CONTOUR", "VARIABLE " + str(scene_contour.value) + "\n"])


def set_scene(scene: utils_command.Scene):
    return scene.value + "\n\n"


def save_scene_as_image(img_path):
    if img_path == "":
        print("no image path")
        raise Exception
    return concatenate(["SAVE_SCENE_AS_IMAGE", img_path + "\n"])

# TODO Create a class for coordinate system and for motion
# have a parameter of class id to know for a new creation of motion what is its id


def create_new_coordinate_system():
    return "CREATE_NEW_COORDINATE_SYSTEM\n"


def edit_coordinate_system(id, origin, rotation_matrix, name="system_"+str(id)):
    return concatenate(["EDIT_COORDINATE_SYSTEM",
                        convert("FRAME", id),
                        convert("NAME", name),
                        convert("ORIGIN_X", origin[0]),
                        convert("ORIGIN_Y", origin[1]),
                        convert("ORIGIN_Z", origin[2]),
                        convert("VECTOR_X_X", rotation_matrix[0, 0]),
                        convert("VECTOR_X_Y", rotation_matrix[0, 1]),
                        convert("VECTOR_X_Z", rotation_matrix[0, 2]),
                        convert("VECTOR_Y_X", rotation_matrix[1, 0]),
                        convert("VECTOR_Y_Y", rotation_matrix[1, 1]),
                        convert("VECTOR_Y_Z", rotation_matrix[1, 2]),
                        convert("VECTOR_Z_X", rotation_matrix[2, 0]),
                        convert("VECTOR_Z_Y", rotation_matrix[2, 1]),
                        convert("VECTOR_Z_Z", rotation_matrix[2, 2])])


def rotate_coordinate_system(angle, frame_id, rotation_frame, rotation_axis):
    return concatenate(["ROTATE_COORDINATE_SYSTEM",
                        convert("FRAME", frame_id),
                        convert("ROTATION_FRAME", rotation_frame),
                        convert("ROTATION_AXIS", rotation_axis),
                        convert("ANGLE", angle)])


def surface_translate(x, y, z, frame_1_id=1, frame_2_id=1, type="VECTOR", surface_id=0, split_vertices=True):
    """type = FRAME or VECTOR"""
    if x == 0 and y == 0 and z == 0:
        return ""
    else:
        return concatenate(["SURFACE_TRANSLATE",
                            convert("FRAME1", frame_1_id),
                            convert("FRAME2", frame_2_id),
                            convert("VECTOR_X", x),
                            convert("VECTOR_Y", y),
                            convert("VECTOR_Z", z),
                            convert("TYPE", type),
                            convert("SURFACE", surface_id),
                            convert("SPLIT_VERTICES", split_vertices)])
