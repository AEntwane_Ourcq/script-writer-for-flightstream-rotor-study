import os
import pickle

import numpy as np
import scipy.io
from scipy.optimize import minimize

from fs_code import flightstream_execute as fs_exe
from fs_code import flightstream_project as fs_p
from fs_code import parser2 as ps
from fs_code import utils
from fs_code import write_script as ws
from fs_code.read_csv_flug import export_data


def define_rotor():
    # upper = utils.Flug_verif(rpms=upper_rpms, ccw=fs_p.UPPER_CCW)
    # lower = utils.Flug_verif(rpms=lower_rpms, ccw=fs_p.LOWER_CCW)

    rotors = [
        utils.Rotor(surfaces=[utils.Surface(index_angle[0], index_angle[1]) for index_angle in fs_p.UPPER_SURFACE_INDEX_ANGLE],
                    pitch_axis="Y", rpm=0, rotation_axis="X", id=1, ccw=fs_p.UPPER_CCW, frame_id=fs_p.UPPER_FRAME_ID),

        utils.Rotor(surfaces=[utils.Surface(index_angle[0], index_angle[1]) for index_angle in fs_p.LOWER_SURFACE_INDEX_ANGLE], pitch_axis="Y", rpm=0, rotation_axis="X", id=2, ccw=fs_p.LOWER_CCW, frame_id=fs_p.LOWER_FRAME_ID)]
    return rotors


def extracting_data(output_file_path, rotors):
    total, upper, lower, info_dict = ps.parse_file_coax_rotor(
        output_file_path, expected_elements=fs_p.NAME_SURFACES)

    sign_upper = 1
    sign_lower = 1
    if not rotors[0].ccw:
        sign_upper = -1
    if not rotors[1].ccw:
        sign_lower = -1

    upper_rpm = upper.rpm
    upper_thrust_newton = -upper.fx
    lower_rpm = lower.rpm
    lower_thrust_newton = -lower.fx
    upper_torque = sign_upper * upper.mx
    upper_power = sign_upper * upper.mx*upper.rpm * utils.RPM2RAD
    lower_torque = sign_lower * lower.mx
    lower_power = sign_lower * lower.mx*lower.rpm * utils.RPM2RAD

    print(info_dict)

    total_thrust_newton = -total.fx
    total_torque = upper_torque + lower_torque
    total_power = upper_power + lower_power

    # print("total_torque: ", total_torque)
    # print("total_power: ", total_power)

    total_thrust_kgf = total_thrust_newton * utils.N2KGF
    ratio_kgf_power = total_thrust_kgf/total_power

    print(str(ratio_kgf_power * 1000) + " g/W")
    return upper_rpm, upper_thrust_newton, lower_rpm, lower_thrust_newton, upper_torque, lower_torque, total_torque, upper_power, lower_power, total_thrust_newton, total_power, ratio_kgf_power, info_dict


def coeff_fom_value(upper_rpm, upper_thrust_newton, lower_rpm, lower_thrust_newton, upper_torque, lower_torque, kgf=True):
    # total_foms = np.sqrt((total_thrusts*KGF2N)**3 / (rho * 2 * np.pi)) / (total_powers * fs_p.DIAMETER/2)
    # total_foms_new = (np.sqrt((np.array(d_f["a_thrusts"])*KGF2N)**3 / (rho * 2 * np.pi)) + np.sqrt((np.array(d_f["b_thrusts"])*KGF2N)**3 / (rho * 2 * np.pi)) ) / (total_powers * fs_p.DIAMETER/2)
    rho = 1.225

    ct_a = upper_thrust_newton / (rho * (upper_rpm/60)**2 * fs_p.DIAMETER**4)
    ct_b = lower_thrust_newton / (rho * (lower_rpm/60)**2 * fs_p.DIAMETER**4)
    cp_a = (upper_torque * upper_rpm * np.pi /
            30) / (rho * (upper_rpm/60)**3 * fs_p.DIAMETER**5)
    cp_b = (lower_torque * lower_rpm * np.pi /
            30) / (rho * (lower_rpm/60)**3 * fs_p.DIAMETER**5)
    cp = cp_a + cp_b
    # old =  1/np.sqrt(2) * (ct_a + ct_b)**(3/2)/cp
    #
    kappa_int_inf = 1.2657
    fom_coax = kappa_int_inf/np.sqrt(2) * (ct_a**(3/2) + ct_b**(3/2))/cp
    return fom_coax


def convert_to_old_set(one_set):
    """old set of 5 free parameters, new set of 3 free parameters (a_rpm, a_pitch fixed) -> b defined in relative to a"""
    a_rpm = fs_p.OLD_SET_INIT[0]
    b_rpm = a_rpm * (1+one_set[0]/100)
    a_pitch = fs_p.OLD_SET_INIT[2]
    b_pitch = one_set[1]
    spacing = one_set[2]
    old_one_set = [a_rpm, b_rpm, a_pitch, b_pitch, spacing]
    return old_one_set


def evaluate(one_set, dirname, rotors, save, n_iterations, variable="FoM", new=False):
    # sets = [angle, freestream, rpma, rpmb, pa, pb, space]
    flightstream_project = fs_p.NAME
    d = fs_p.DIAMETER
    steady = False

    ref_area = 1  # Not really useful if take directly the force and moment and not the coefficient form FlightStream

    # PATH ROUTINE
    flightstream_file = os.path.join(
        "flightstream_files", flightstream_project)

    str_steady = "steady"
    if not steady:
        str_steady = "un" + str_steady
    already_calculated = False

    for ind, e in enumerate(save):
        if not (one_set - e).any():
            already_calculated = True
            print(one_set, "already calculated:",
                  ind+1, "/", len(save))
            break
    if new:
        # conversion to old new format
        old_one_set = convert_to_old_set(one_set)
    else:
        old_one_set = one_set
    input_name, output_file_path = ws.optimization(
        steady, dirname, flightstream_file, old_one_set, rotors, d, ref_area, coax=fs_p.COAX, pitch=fs_p.PITCH, spacing=fs_p.SPACING, already_calculated=already_calculated)
    # print("write script coax done: ", input_name.split("\\")[-1])
    if not already_calculated:
        fs_exe.main_list(dirname, [input_name])
        save.append(one_set)

    upper_rpm, upper_thrust_newton, lower_rpm, lower_thrust_newton, upper_torque, lower_torque, total_torque, upper_power, lower_power, total_thrust_newton, total_power, ratio_kgf_power, info_dict = extracting_data(
        output_file_path, rotors)

    n_iterations[0] += 1
    print("evaluation iterations:", n_iterations)
    with open('./pkl_save/opt_save_' + str(n_iterations[0]) + '.pkl', 'wb') as f:
        pickle.dump(save, f)
    # if variable == "kgf/W":
    #     return -ratio_kgf_power
    if variable == "FoM":

        fom = coeff_fom_value(upper_rpm, upper_thrust_newton, lower_rpm,
                              lower_thrust_newton, upper_torque, lower_torque,)
        print(fom)
        return - fom
    else:
        print(variable + " is not used to optimize")
        raise Exception


def main(dirname, pkl):
    new = True
    if not new:
        one_set = fs_p.OLD_SET_INIT
    else:
        new_one_set = fs_p.NEW_SET_INIT
    rotors = define_rotor()
    if not new:
        x_bounds = [fs_p.RPM_BOUNDS, fs_p.RPM_BOUNDS,
                    fs_p.PITCH_BOUNDS, fs_p.PITCH_BOUNDS, fs_p.SPACING_BOUNDS]
    else:
        new_x_bounds = [fs_p.RELATIVE_RPM_BOUNDS,
                        fs_p.PITCH_BOUNDS, fs_p.SPACING_BOUNDS]

    n_iterations_max = 50
    save = []
    n_iterations = [0]
    try:
        save = load_data(pkl)
        print("load pickle ok")
    except:
        save = []
        print("pickle exception")
    # result = minimize(evaluate, one_set, args=(dirname, rotors, save), method="Nelder-Mead",
    #                   bounds=x_bounds, constraints=nlc, options={'maxiter': n_iterations_max, 'maxfev': n_iterations_max})

    result = minimize(evaluate, new_one_set, args=(dirname, rotors, save, n_iterations, "FoM", new), method="Nelder-Mead",
                      bounds=new_x_bounds, options={'maxiter': n_iterations_max, 'maxfev': n_iterations_max})
    print(result)
    # ratio_kgf_power = evaluate(one_set, dirname, rotors)
    # print(str(ratio_kgf_power * 1000) + " g/W")

    with open('./pkl_save/opt_save_final.pkl', 'wb') as f:
        pickle.dump(save, f)


def load_data(file):  # ='./pkl_save/opt_save_24.pkl'):
    with open(file, "rb") as f:
        save = pickle.load(f)
    return save


def analysis(dirname, file):  # ='opt_save_25.pkl'):
    save = load_data(file=dirname+"./pkl_save/" + file)

    upper_rpms, upper_thrust_newtons, lower_rpms, lower_thrust_newtons, upper_powers, lower_powers, total_thrust_newtons, total_powers, ratio_kgf_powers = [], [], [], [], [], [], [], [], []
    upper_torques, lower_torques = [], []
    upper_pitchs, lower_pitchs, spacings = [], [], []
    for one_set in save:
        one_set = convert_to_old_set(one_set)

        print(one_set)
        rotors = define_rotor()

        flightstream_project = fs_p.NAME
        d = fs_p.DIAMETER
        steady = False

        ref_area = 1  # Not really useful if take directly the force and moment and not the coefficient form FS

        # PATH ROUTINE
        flightstream_file = os.path.join(
            "flightstream_files", flightstream_project)

        str_steady = "steady"
        if not steady:
            str_steady = "un" + str_steady

        input_name, output_file_path = ws.optimization(
            steady, dirname, flightstream_file, one_set, rotors, d, ref_area, coax=fs_p.COAX, pitch=fs_p.PITCH, spacing=fs_p.SPACING, already_calculated=True)
        try:

            upper_rpm, upper_thrust_newton, lower_rpm, lower_thrust_newton, upper_torque, lower_torque, total_torque, upper_power, lower_power, total_thrust_newton, total_power, ratio_kgf_power, info_dict = extracting_data(
                output_file_path, rotors)

            upper_rpms.append(upper_rpm)
            upper_thrust_newtons.append(upper_thrust_newton)
            lower_rpms.append(lower_rpm)
            lower_thrust_newtons.append(lower_thrust_newton)
            upper_powers.append(upper_power)
            lower_powers.append(lower_power)
            total_thrust_newtons.append(total_thrust_newton)
            total_powers.append(total_power)
            ratio_kgf_powers.append(ratio_kgf_power)
            upper_torques.append(upper_torque)
            lower_torques.append(lower_torque)

            # dict mangement:
            upper_pitchs.append(info_dict["a_pitch"])
            lower_pitchs.append(info_dict["b_pitch"])
            spacings.append(info_dict["spacing"])
        except FileNotFoundError as e:
            print(output_file_path + " SKIPPED")
            print(e)

    # conversion for flightstream analysis
    a_rpms = upper_rpms
    a_thrusts = upper_thrust_newtons
    a_torques = upper_torques
    a_powers = upper_powers
    b_rpms = lower_rpms
    b_thrusts = lower_thrust_newtons
    b_torques = lower_torques
    b_powers = lower_powers
    # total_thrust_newtons
    # total_powers
    # ratio_kgf_powers
    print("saving scipy")
    print(a_rpms)
    print(upper_pitchs)

    export_dict = dict(a_rpms=a_rpms, a_thrusts=a_thrusts, a_torques=a_torques, b_rpms=b_rpms, b_thrusts=b_thrusts, b_torques=b_torques,
                       a_powers=a_powers, b_powers=b_powers, ratio_kgf_powers=ratio_kgf_powers, a_pitchs=upper_pitchs, b_pitchs=lower_pitchs, spacings=spacings)
    scipy.io.savemat("flightstream_opti.mat", export_dict)
    return export_dict
