import fnmatch
import os

import matplotlib.pyplot as plt
import numpy as np

from fs_code import parser2 as ps


def ls_outputs_txt(dirname="."):
    outputs = []
    output_str = os.path.join(dirname, "outputs")
    list_of_angle_folders = os.listdir(output_str)
    for angle_folder in list_of_angle_folders:
        list_of_files = os.listdir(os.path.join(output_str, angle_folder))
        txt_pattern = "j_[0-9.]*.txt"
        for file in list_of_files:
            if fnmatch.fnmatch(file, txt_pattern):
                print(file)
                outputs.append(os.path.join(angle_folder, file))
    return outputs


def flug_outputs_txt(dirname):
    outputs = []
    output_str = os.path.join(dirname, "outputs")
    list_of_files = os.listdir(output_str)
    txt_pattern = "*.txt"
    for file in list_of_files:
        if fnmatch.fnmatch(file, txt_pattern):
            print(file)
            outputs.append(os.path.join(output_str, file))
    return outputs


def open_ref(path):
    xs_ref = []
    ys_ref = []
    with open(path, "r") as f:  # TODO create proper reference folder for each cases
        for line in f:
            if "," in line:
                l = line.split(",")
            else:
                l = line.split()
            x, y = float(l[0]), float(l[1])
            xs_ref.append(x)
            ys_ref.append(y)
    return xs_ref, ys_ref


def open_and_plot_ref(path, ax):
    xs_ref, ys_ref = open_ref(path)
    ax.plot(xs_ref, ys_ref, "k.", label="reference")


def plot_figure(xs, ys, labels, xlabel=r"$j$", ylabel=r"$C$", title="", dirname="", reference: bool = False):
    """dirname used for printing reference data"""
    fig, ax = plt.subplots()
    # TODO create proper reference to return to the folder of naca_csv
    if dirname != "":
        print("dirname for references plotting: ", dirname)
        naca_ref_dir = dirname
    for i in range(len(labels)):
        # angle = "angle " + str(angle_values[i]) + "°",
        ax.plot(xs[i], ys[i], ".--", label=labels[i])

    if ylabel == r"$C_P$":
        max_y = 0.30
        step_y_major = 0.02
        step_y_minor = 0.01
        max_x = 3.4
        step_x_major = 0.2
        step_x_minor = 0.1
        if reference:
            open_and_plot_ref(os.path.join(
                naca_ref_dir, "cp_plot_digitizer.csv"), ax)
    elif ylabel == r"$C_T$":
        max_y = 0.15
        step_y_major = 0.02
        step_y_minor = 0.01
        max_x = 2.8
        step_x_major = 0.2
        step_x_minor = 0.1
        # img = plt.imread(dirname + "/save/reference_ct_v2.png")
        # ax.imshow(img)#, extent=[0, 3, 0, 0.10]) #0.15
        if reference:
            open_and_plot_ref(os.path.join(
                naca_ref_dir, "ct_reference_cleaned.csv"), ax)
    elif ylabel == r"$\eta$":
        max_y = 1
        step_y_major = 0.2
        step_y_minor = 0.1
        max_x = 2.8
        step_x_major = 0.2
        step_x_minor = 0.1
        if reference:
            open_and_plot_ref(os.path.join(
                naca_ref_dir, "efficiency_plot_digitizer_cleaned.csv"), ax)
    else:
        raise Exception

    ax.set_xlim(0, max_x)
    ax.set_ylim(0, max_y)

    major_ticks_y = np.arange(0, max_y+step_y_minor, step_y_major)
    minor_ticks_y = np.arange(0, max_y+step_y_minor, step_y_minor)
    major_ticks_x = np.arange(0, max_x+step_x_minor, step_x_major)
    minor_ticks_x = np.arange(0, max_x+step_x_minor, step_x_minor)
    ax.set_xticks(major_ticks_x)
    ax.set_yticks(major_ticks_y)
    ax.set_xticks(minor_ticks_x, minor=True)
    ax.set_yticks(minor_ticks_y, minor=True)
    ax.grid(which="major", alpha=0.6)
    ax.grid(which="minor", alpha=0.3)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.legend()


def naca_main(dirname, steady, to_write=False):
    dirname_steady = ""
    root_dir = os.path.join(os.path.dirname(__file__), "..")
    print("dirname output : ", root_dir)
    ref_dir = os.path.join(os.path.join(
        os.path.dirname(__file__), ".."), "naca_references")

    if steady:
        dirname_steady = os.path.join(dirname, "steady")
    else:
        dirname_steady = os.path.join(dirname, "unsteady")
    files_to_analyse = ls_outputs_txt(dirname_steady)
    # files = [os.path.join(dirname_steady, file) for file in files_to_analyse]
    files = files_to_analyse[:]
    out_list = []
    js = []
    cts = []
    cps = []
    efficiencies = []
    angle_values = []
    for exp_file in files:
        exp_file = exp_file.strip("\n")
        l1 = 0
        angle_value = int(
            exp_file[l1 + len("angle_"): l1 + len("angle_") + 2])
        if angle_value in angle_values:
            angle_index = angle_values.index(angle_value)
        else:
            angle_values.append(angle_value)
            js.append([])
            cts.append([])
            cps.append([])
            efficiencies.append([])
            angle_index = len(angle_values) - 1

        out_file_relative = "outputs/" + exp_file

        out_file = os.path.join(dirname_steady, out_file_relative)
        data = ps.parse_file_simple_rotor(out_file, angle_value)
        out_list.append(data.__repr__())
        js[angle_index].append(data.j)
        cts[angle_index].append(data.ct)
        cps[angle_index].append(data.cp)
        efficiencies[angle_index].append(data.efficiency)

    if to_write:
        with open(os.path.join(dirname_steady, "full_data.csv"), "w+") as f:
            if steady:
                f.write("steady\n")
            else:
                f.write("unsteady\n")
            f.write("j, ct, cp, efficiency\n")
            for line in out_list:
                f.write(line + "\n")
    labels = []

    title = ""
    if steady:
        title = "Steady Set"
    else:
        title = "Unsteady Set"

    labels = ["angle: " + str(angle) + "°" for angle in angle_values]
    plot_figure(js, cts, labels, xlabel=r"$j$",
                ylabel=r"$C_T$", title=title + " - Ct", dirname=ref_dir, reference=True)
    plot_figure(js, cps, labels, xlabel=r"$j$",
                ylabel=r"$C_P$", title=title + " - Cp", dirname=ref_dir, reference=True)
    plot_figure(js, efficiencies, labels, xlabel=r"$j$",
                ylabel=r"$\eta$", title=title + " - efficiency", dirname=ref_dir, reference=True)
    plt.show()

    with open(os.path.join(dirname_steady, "data.csv"), "w+") as f:
        f.write("solver mode,")
        steady_str = "steady"
        if not steady:
            steady_str = "un" + steady_str
        f.write(steady_str + "\n")
        f.write("j,ct,cp,efficiency,label\n")
        for i in range(len(angle_values)):
            for j in range(len(js[i])):
                f.write(str(js[i][j]) + "," + str(cts[i][j]) + "," + str(cps[i][j]) + "," + str(
                    efficiencies[i][j]) + "," + steady_str + " " + str(angle_values[i]) + " deg\n")


def main(dirname, steady, to_write=False):
    dirname_steady = ""
    if steady:
        dirname_steady = os.path.join(dirname, "steady")
    else:
        dirname_steady = os.path.join(dirname, "unsteady")
    files = flug_outputs_txt(dirname_steady)
    out_list = []
    js = []
    cts = []
    cps = []
    efficiencies = []
    angle_values = []
    for exp_file in files:
        out_file = exp_file.strip("\n")
        angle_value = 0
        data = ps.parse_file_simple_rotor(out_file, angle_value)
        out_list.append(data.__repr__())
        js.append(data.j)
        cts.append(data.ct)
        cps.append(data.cp)
        efficiencies.append(data.efficiency)

    if to_write:
        with open(os.path.join(dirname_steady, "full_data.csv"), "w+") as f:
            if steady:
                f.write("steady\n")
            else:
                f.write("unsteady\n")
            f.write("j, ct, cp, efficiency\n")
            for line in out_list:
                f.write(line + "\n")
    labels = []

    title = ""
    if steady:
        title = "Steady Set"
    else:
        title = "Unsteady Set"

    labels = ["angle: " + str(angle) + "°" for angle in angle_values]
    plot_figure(js, cts, labels, xlabel=r"$j$",
                ylabel=r"$C_T$", title=title + " - Ct", dirname=dirname)
    plot_figure(js, cps, labels, xlabel=r"$j$",
                ylabel=r"$C_P$", title=title + " - Cp")
    plot_figure(js, efficiencies, labels, xlabel=r"$j$",
                ylabel=r"$\eta$", title=title + " - efficiency")
    # plt.show()

    with open(os.path.join(dirname_steady, "data.csv"), "w+") as f:
        f.write("solver mode,")
        steady_str = "steady"
        if not steady:
            steady_str = "un" + steady_str
        f.write(steady_str + "\n")
        f.write("j,ct,cp,efficiency,label\n")
        for i in range(len(angle_values)):
            for j in range(len(js[i])):
                f.write(str(js[i][j]) + "," + str(cts[i][j]) + "," + str(cps[i][j]) + "," + str(
                    efficiencies[i][j]) + "," + steady_str + " " + str(angle_values[i]) + " deg\n")


def thrust_power_analysis(dirname, steady, to_write=False, coax=False):
    dirname_steady = ""
    if steady:
        dirname_steady = os.path.join(dirname, "steady")
    else:
        dirname_steady = os.path.join(dirname, "unsteady")
    flugauto = True
    files_to_analyse = flug_outputs_txt(dirname_steady)
    if not flugauto:
        files_to_analyse = ls_outputs_txt(dirname_steady)

    files = files_to_analyse

    out_list = []
    thrusts = []
    powers = []
    rpms = []
    for exp_file in files:
        out_file = exp_file.strip("\n")
        # print("outfile: " + out_file)
        angle_value = 0
        if not coax:
            data = ps.parse_file_simple_rotor(out_file, angle_value)
            out_list.append(data.__repr__())
            thrusts.append(data.thrust)
            powers.append(data.power)
            rpms.append(data.rpm)
        else:
            exp_elmts = [["blade_1_1", "blade_1_2", "fuselage_1"],
                         ["blade_2_1", "blade_2_2", "fuselage_2"]]
            total_result, upper_result, lower_result = ps.parse_file_coax_rotor(
                out_file, expected_elements=exp_elmts)
    title = ""
    if steady:
        title = "Steady Set"
    else:
        title = "Unsteady Set"

    # labels = ["angle: " + str(angle) + "°" for angle in angle_values]

    def simple_plot_figure(xs, ys, labels, xlabel, ylabel, title):
        fig, ax = plt.subplots()
        for i in range(len(labels)):
            # angle = "angle " + str(angle_values[i]) + "°",
            ax.plot(xs[i], ys[i], ".--", label=labels[i])
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_title(title)
        ax.legend()

    # simple_plot_figure(rpms, thrusts,["lab" for i in rpms], xlabel=r"$rpm$",
    #             ylabel=r"$T$", title=title + " - Thrust")
    # simple_plot_figure(rpms, powers,["lab" for i in rpms], xlabel=r"$rpm$",
    #             ylabel=r"$P$", title=title + " - Power")

    # plot_figure(rpms, efficiencies, labels, xlabel=r"$rpm$",
    #             ylabel=r"$\eta$", title=title + " - efficiency")
    plt.show()
    steady_str = "steady"
    if not steady:
        steady_str = "un" + steady_str

    thrust_kgf = [thrust * 0.101972 for thrust in thrusts]

    print("all rpms : ", rpms)
    with open(os.path.join(dirname_steady, steady_str + "_data.csv"), "w+") as f:
        f.write("rpm, thrust (N), thrust (kgf), power (W)\n")
        for i in range(len(thrusts)):
            f.write(str(rpms[i]) + "," + str(thrusts[i]) + "," +
                    str(thrust_kgf[i]) + "," + str(powers[i]) + "\n")
    
    # with open(os.path.join(dirname_steady, "data.csv"), "w+") as f:
    #     f.write("solver mode,")
    #     steady_str = "steady"
    #     if not steady:
    #         steady_str = "un" + steady_str
    #     f.write(steady_str + "\n")
    #     f.write("rpm, thrust, power\n")
    #     # for i in range(len(angle_values)):
    #     for j in range(len(rpms)):
    #         f.write(str(rpms[j]) + "," + str(thrusts[j]) +
    #                 "," + str(powers[j]) + "\n")

    return rpms, thrusts, powers




def thrust_power_analysis_coax(dirname, steady, to_write=False):
    def simple_plot_figure(xs, ys, labels, xlabel, ylabel, title):
        fig, ax = plt.subplots()
        for i in range(len(labels)):
            # angle = "angle " + str(angle_values[i]) + "°",
            ax.plot(xs[i], ys[i], ".--", label=labels[i])
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_title(title)
        ax.legend()

    dirname_steady = ""
    if steady:
        dirname_steady = os.path.join(dirname, "steady")
    else:
        dirname_steady = os.path.join(dirname, "unsteady")
    files_to_analyse = flug_outputs_txt(dirname_steady)
    print("files to analyse: ", len(files_to_analyse))
    total_results, upper_results, lower_results = [], [], []
    i = 0
    for exp_file in files_to_analyse:
        i += 1
        exp_file = exp_file.strip("\n")
        out_file = exp_file
        exp_elmts = [["blade_1_upper", "blade_2_upper", "fuselage_upper"],
                     ["blade_1_lower", "blade_2_lower", "fuselage_lower"]]  # new version of FS soft with previous setup
        # exp_elmts = [["blade_1_upper", "blade_2_upper"], # if hub/fuselage is broken
        #              ["blade_1_lower", "blade_2_lower"]]
        # exp_elmts = [["Prop-A"],["Prop-B"]] # new version with hub
        total_result, upper_result, lower_result = ps.parse_file_coax_rotor(
            out_file, expected_elements=exp_elmts)
        total_results.append(total_result)
        upper_results.append(upper_result)
        lower_results.append(lower_result)
        # print("upper mx power analyis: ", upper_result.mx)
        # print("lower mx power analyis: ", upper_result.mx)
    print(i)
    return total_results, upper_results, lower_results


def optimization_analysis(dirname, steady, input_file_path, to_write=False):
    dirname_steady = ""
    if steady:
        dirname_steady = os.path.join(dirname, "steady")
    else:
        dirname_steady = os.path.join(dirname, "unsteady")
    print("files to analyse: ", input_file_path)

    exp_elmts = [["Prop-A"], ["Prop-B"], ["Prop-A-blades"], ["Prop-B-blades"]]
    total_result, upper_result, lower_result = ps.parse_file_coax_rotor(
        input_file_path, expected_elements=exp_elmts)
    return total_result, upper_result, lower_result


def plot_csv(dirname):
    js = []
    cts = []
    with open(os.path.join(dirname, "full_data.csv"), "r") as f:
        c = 0
        for line in f:
            if c < 2:
                c += 1  # skip title and labels
                continue
            else:
                line_listed = line.strip("\n").split(', ')
                js.append(float(line_listed[0]))
                cts.append(-float(line_listed[1]))
    plt.plot(js, cts, ".")
    plt.grid()
    plt.show()
