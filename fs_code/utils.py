import os

from fs_code import flightstream_commands as fc

RPM2RAD = 0.10472
N2KGF = 0.101972


class Surface():
    def __init__(self, index, angle=0):
        self.index = index
        self.angle = angle

    def update(self, angle):
        self.angle = angle


class Rotor():
    def __init__(self, surfaces=[], pitch_axis: str = "Y", rpm: float = 0, rotation_axis: str = "X", id=0, ccw=True, frame_id=1):
        self.id = id
        # pitch axis well defined only for two bladed rotor TODO change that to support rotation of the pitch angle for other configurations than 2-blade-rotor
        self.surfaces = surfaces
        self.pitch_axis = pitch_axis
        self.rpm = rpm
        self.rotation_axis = rotation_axis
        self.ccw = ccw  # sense of rotation
        self.frame_id = frame_id

    def update_surface_angle(self, angle: float):
        for surface in self.surfaces:
            surface.update(angle)

    def geometry_unite(self, dirname):
        dir_openvsp = os.path.join(dirname, os.path.join(
            "flightstream_files", "OpenVSP_path.txt"))
        openvsp_path = ""
        with open(dir_openvsp) as f:
            openvsp_path = f.read()
        return fc.geometry_unite(openvsp_path, [surface.index for surface in self.surfaces])

    def create_motion(self, angular_accelerations=[0, 0, 0], start_time=0, frame_id=1):
        # TODO define other cases
        angular_velocities = [0, 0, 0]
        if self.rotation_axis == "X":
            angular_velocities[0] = self.rpm * RPM2RAD
            if self.ccw:
                angular_velocities[0] *= -1
        if self.id == 0:
            raise Exception
        return fc.create_rotor_motion(self.id, self.rotation_axis, [surface.index for surface in self.surfaces], angular_velocities, angular_accelerations, start_time, True, True)


class Flug_verif():  # TODO Naming...

    def __init__(self, rpms=[], thrusts_newton=[], thrusts_kgf=[], powers=[], torques=[], ccw=True, exp=False):
        self.rpms = rpms
        self.thrusts_newton = thrusts_newton
        self.thrusts_kgf = thrusts_kgf
        self.torques = torques
        self.ccw = ccw  # sense of rotation
        if not exp:
            if self.thrusts_newton == []:
                self.thrusts_newton = self.kgf_to_newton()
            elif self.thrusts_kgf == []:
                self.thrusts_kgf = self.newton_to_kgf()
            else:
                raise Exception()
        self.powers = powers

    def newton_to_kgf(self):
        print(self.thrusts_newton, self.thrusts_kgf)
        self.thrusts_kgf = [thrust * N2KGF for thrust in self.thrusts_newton]
        return [thrust * N2KGF for thrust in self.thrusts_newton]

    def kgf_to_newton(self):
        return [thrust / N2KGF for thrust in self.thrusts_kgf]

    # def add_newton_thrusts(self, ts):
    #     self.thrusts_newton = ts
    #     self.thrusts_kgf = self.newton_to_kgf()

    # def add_kgf_thrusts(self, ts):
    #     self.thrusts_kgf = ts
    #     self.thrusts_newton = self.kgf_to_newton()

    def __repr__(self):
        l = "[ "

        def print_list(lab, l):
            a = lab
            for e in l:
                a = a + str(e) + ","
            return a + ";"

        l += print_list("rpm ", self.rpms)
        l += print_list("\nthrusts ", self.thrusts_kgf)
        l += print_list("\npowers ", self.powers)
        return "flug verif: " + l + "]\n"


class Flug_verif_coax():  # TODO Naming...
    def __init__(self, upper: Flug_verif, lower: Flug_verif):
        self.upper = upper
        self.lower = lower
