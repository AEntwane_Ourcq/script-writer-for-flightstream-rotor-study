NAME =  "" #<".fsm">

DIAMETER = 0 #<float>
BASE_PITCH_ANGLE = 0  # <float> # NACA 15  # deg
H_SPACE = 0  # <float> # periodicity, the real space separation is H_SPACE - L_HUB
ROTOR_SIZE = 0  # <float> # in meter
COAX = True

UPPER_CCW = True
LOWER_CCW = not UPPER_CCW

UPPER_FRAME_ID = 3
LOWER_FRAME_ID = 4

UPPER_SURFACE_INDEX_ANGLE = [(1, 0), (3, 0)]
LOWER_SURFACE_INDEX_ANGLE = [(2, 0), (4, 0)]

HUB_SURFACE = [1, 2]

NAME_SURFACES = [["Prop-A", "Prop-A-blades"], ["Prop-B", "Prop-B-blades"]]

# for optimization
PITCH = True
PITCH_BOUNDS = (0, 0)  # (<float>, <float>)
SPACING = True
SPACING_BOUNDS = (ROTOR_SIZE, ROTOR_SIZE*2)  # <float> #in meter
RPM_BOUNDS = (0, 0)  # (int,int)
# (<float>,<float>) # for the NEW_SET_INIT b_rpm (in percent)
RELATIVE_RPM_BOUNDS = (-0, 0)

# [<int>, <int> ,<float>, <float>, <float>] # a_rpm, b_rpm, a_pitch, b_pitch, periodic_spacing
OLD_SET_INIT = [0, 0, 0, 0, 0]
# [<float>, <float>, <float>] # b_rpm express in relative difference from a_rpm (ex: -10 leads to b_rpm = (1 - 10/100)a_rpm), b_pitch, spacing
NEW_SET_INIT = [0, 0, 0]

ABS_MAX_RPM = 0  # <int>

# validation
validation_simple_rotor = {"rpms": [], "thrusts_kgf": [], "powers": []}
