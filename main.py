import fnmatch
import os
import sys
import time

import matplotlib.pyplot as plt
import numpy as np
from scipy.io.matlab.mio import loadmat

from contour_map import extract_data
from fs_code import flightstream_execute as fs_exe
from fs_code import flightstream_project as fs_p
from fs_code import flugauto_optimization as fl_opt
from fs_code import output_analysis as out_ana
from fs_code import read_csv_flug as csvf
from fs_code import test_matrix_reader as tm_r
from fs_code import utils
from fs_code import write_script as ws

NACA_FLIGHTSTREAM_PROJECT = "Test_unsteady_solver_-_no_motion.fsm"


def naca_main(dirname, steady, periodic):
    # naca report 640
    start_time = time.time()
    js_extremum = []
    rpms = []
    angles = []
    flightstream_file = ""
    #  generating all conditions
    js_extremum = [[0.2, 0.9], [0.2, 1.1], [0.2, 1.3],
                   [0.2, 1.6], [0.2, 1.8], [0.2, 2.2], [0.2, 2.7]]
    rpms = [1200, 1200, 1000, 1000, 800, 900, 800]
    # 15 to 45 deg (minus 15 due to initial rotation from the project)
    angles = [i-15 for i in range(15, 46, 5)]

    # linear part:
    js_extremum = [[0.2, 0.82], [0.4, 1.05], [0.5, 1.3], [
        0.8, 1.65], [1.0, 1.82], [1.2, 2.2], [1.5, 2.65]]

    # extract conditions for one point
    # index = angles.index(30-15)
    # angles = [angles[index]]
    # js_extremum = [js_extremum[index]]
    # rpms = [rpms[index]]

    # extract conditions for the rest points
    start_index = 6
    end_index = len(rpms)
    angles = angles[start_index:end_index]
    js_extremum = js_extremum[start_index:end_index]
    rpms = rpms[start_index:end_index]

    js_extremum = [[1.8, 2.1]]

    if dirname == "":
        dirname = os.path.dirname(__file__)
    flightstream_file = os.path.join(
        "flightstream_files", NACA_FLIGHTSTREAM_PROJECT)
    if steady:
        print("steady mode")
    else:
        print("unsteady mode")
        # flightstream_file = "flightstream_files/Test_unsteady_solver_-_no_motion.fsm"
    nb_points_one_angle = 5
    # list_script_file = ws.naca_main(steady, dirname, flightstream_file, js_extremum,
    #                           rpms, angles, periodic=periodic, nb_j_points=nb_points_one_angle)
    # fs_exe.main_list_file(dirname, list_script_file)
    # should had steady in file instead of this arg? TODO
    out_ana.naca_main(dirname, steady, to_write=False)
    duration = time.time() - start_time
    print("programme duration: " + str(duration//60) + " min")


def ls_outputs_txt(dirname="."):
    outputs = []
    output_str = os.path.join(dirname, "scripts")
    # list_of_angle_folders = os.listdir(output_str)
    # for angle_folder in list_of_angle_folders:
    list_of_files = os.listdir(output_str)
    txt_pattern = "[a-z-0-9_.]*.txt"
    for file in list_of_files:
        if fnmatch.fnmatch(file, txt_pattern):
            # print("file selected : ", file)
            outputs.append(os.path.join(output_str, file))
    print("len scripts: ", len(outputs))
    return outputs


def flugauto_main(dirname):
    start_time = time.time()
    flightstream_file = ""

    if dirname == "":
        dirname = os.path.dirname(__file__)
    flightstream_file = os.path.join(
        "flightstream_files", "the_union.fsm")

    steady = False
    sets, d = tm_r.main(single_rotor=True)

    # _ = ws.flugauto_text_matrix_main(steady, dirname, flightstream_file, sets, d, ref_area)
    # list_script_files = ls_outputs_txt(os.path.join(dirname,"unsteady"))
    # print(list_script_files)
    # fs_exe.main_list(dirname, list_script_files)

    out_ana.main(dirname, steady, to_write=False)
    duration = time.time() - start_time
    print("programme duration: " + str(duration//60) + " min")


def flugauto_validation_simple_rotor(dirname, steady=False):
    flightstream_project = "single_rotor.fsm"
    d = fs_p.DIAMETER
    rpms_ref = fs_p.validation_simple_rotor["rpms"]
    ref = utils.Flug_verif(rpms=rpms_ref,
                           thrusts_kgf=fs_p.validation_simple_rotor["thrusts_kgf"],
                           powers=fs_p.validation_simple_rotor["powers"])
    # exp = utils.Flug_verif(rpms=rpms_ref, exp=True)

    # print("ref: ", ref)
    # print("exp : ", exp)
    flugauto_verification(dirname, flightstream_project,
                          d, ref, steady)


def coax_flug_verif(dirname, steady=False):

    def coax_ls_ref(dirname="."):
        outputs = []
        print(dirname)
        list_of_files = os.listdir(dirname)
        print(list_of_files)
        csv_pattern = "*.csv"
        for file in list_of_files:
            if fnmatch.fnmatch(file, csv_pattern):
                print(file)
                outputs.append(os.path.join(dirname, file))
        return outputs

    dir = ""  # TODO define folder
    join_dir = csvf.recursive_path_join(
        [os.path.dirname(__file__), "flugauto_files", dir])
    coaxdirs = coax_ls_ref(join_dir)
    if len(coaxdirs) == 1:
        coaxfile = coaxdirs[0]
    elif csvf.recursive_path_join([join_dir, "merging_all_data_purged.csv"]) in coaxdirs:
        # TODO change this...
        coaxfile = "merging_all_data_purged.csv"
    else:
        print("coaxdirs:", coaxdirs)
        print("Should be only one csv or CHANGE THIS PART...")
    upper, lower = csvf.coax_rotor(coaxfile, ccws=[True, False])
    print(upper, lower)
    ref_rotor = utils.Flug_verif_coax(upper, lower)
    flugauto_coax_rotors_verification(dirname, steady, ref_rotor)


def flugauto_coax_rotors_verification(dirname, steady=False, ref_rotor=None):
    flightstream_project = fs_p.NAME
    d = fs_p.DIAMETER
    # exp_upper = utils.Flug_verif(rpms =[] , exp = True)
    # exp_lower = utils.Flug_verif(rpms =[] , exp = True)
    # exp_rotor = utils.Flug_verif_coax(exp_upper, exp_lower)
    if ref_rotor == None:
        # TODO (cleaned for public repo)
        rpms_upper = []
        rpms_lower = []  # TODO idea for CW and CCW ?
        ref_upper = utils.Flug_verif(rpms=rpms_upper,
                                     thrusts_kgf=[],
                                     powers=[],
                                     ccw=True)
        ref_lower = utils.Flug_verif(rpms=rpms_lower,
                                     thrusts_kgf=[],
                                     powers=[],
                                     ccw=False)

        ref_rotor = utils.Flug_verif_coax(ref_upper, ref_lower)
    flugauto_verification(dirname, flightstream_project, d, ref_rotor, steady)


def flugauto_verification(dirname, flightstream_project, d, ref, steady=False):

    ref_area = 1  # Not really useful if take directly the force and moment and not the coefficient form FS

    # plotting with ref
    def flug_plot(ref, exp, pre_title):
        def one_plot(exp_x, exp_y, ref_x, ref_y, title, xlabel="", ylabel=""):
            fig, ax = plt.subplots()

            # print("exp :")
            # print(exp_x)
            # print(exp_y)

            # print("ref :")
            # print(ref_x)
            # print(ref_y)

            ax.plot(exp_x[:-1], exp_y[:-1], "b.",
                    ref_x[:-1], ref_y[:-1], "r--")
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)
            ax.legend(["exp", "ref"])
            ax.set_title(title)

            plt.savefig(os.path.join(dirname, title + '.png'))

        if type(exp.rpms[0]) == type(""):
            one_plot(exp.rpms, exp.thrusts_kgf, ref.rpms,
                     ref.thrusts_kgf, pre_title + "Thrust", xlabel="rpm", ylabel="kgf")
            one_plot(exp.rpms, exp.powers, ref.rpms, ref.powers,
                     pre_title + "Power", xlabel="rpm", ylabel="W")

        else:
            one_plot([abs(rpm) for rpm in exp.rpms], exp.thrusts_kgf, [abs(rpm) for rpm in ref.rpms],
                     ref.thrusts_kgf, pre_title + "Thrust", xlabel="rpm", ylabel="kgf")
            one_plot([abs(rpm) for rpm in exp.rpms], exp.powers, [abs(
                rpm) for rpm in ref.rpms], ref.powers, pre_title + "Power", xlabel="rpm", ylabel="W")
        plt.show()

    flightstream_file = os.path.join(
        "flightstream_files", flightstream_project)

    str_steady = "steady"
    if not steady:
        str_steady = "un" + str_steady
    if type(ref) == utils.Flug_verif:
        print("in simple flug_verif")

        rotors = [
            utils.Rotor(surfaces=[utils.Surface(1, 0), utils.Surface(2, 0), utils.Surface(
                3, 0)], pitch_axis="Y", rpm=0, rotation_axis="X", id=1)
        ]

        # sets = tm_r.test_matrix_rpm_only(rpms=exp.rpms, j=0.001)
        freestream_velocity = 0.001
        sets = [(0, freestream_velocity, rpm) for rpm in ref.rpms]
        # ws.flugauto_text_matrix_main(steady, dirname, flightstream_file, sets, rotors, d, ref_area)

        # list_script_files = ls_outputs_txt(os.path.join(dirname, str_steady))
        # fs_exe.main_list(dirname, list_script_files)

        rpms, thrusts_newton, powers = out_ana.thrust_power_analysis(
            dirname, steady, to_write=False)
        print("output of analysis part")
        print(rpms)
        print(thrusts_newton)
        print(powers)
        print("-------")
        # exp.rpms = rpms
        # exp.add_newton_thrusts(thrusts_newton)
        # exp.powers = powers
        # exp.rpms, exp.thrusts_newton, exp.powers = out_ana.thrust_power_analysis(
        #    dirname, steady, to_write=False)

        exp = utils.Flug_verif(
            rpms, thrusts_newton=thrusts_newton, powers=powers)

        flug_plot(ref, exp)

        def csv_writing():
            steady_str = "steady"
            if not steady:
                steady_str = "un" + steady_str
            with open(os.path.join(dirname, os.path.join(steady_str, "simple_rotor_data.csv")), "w+") as f:
                f.write(
                    "ref rpm, ref thrust (N), ref power (W), exp thrust (N), exp power (W), rel diff thrust, rel diff power\n")
                for i in range(len(ref.rpms)):
                    f.write(
                        f"{ref.rpms[i]:5.0f}" + ", "
                        f"{ref.thrusts_newton[i]:5.5f}" + ", "
                        f"{ref.powers[i]:5.5f}" + ", "
                        f"{exp.thrusts_newton[i]:5.5f}" + ", "
                        f"{exp.powers[i]:5.5f}" + ", "
                        f"{(ref.thrusts_newton[i]-exp.thrusts_newton[i])/ref.thrusts_newton[i]:1.5f}" + ", "
                        f"{(ref.powers[i]-exp.powers[i])/ref.powers[i]:1.5f}\n"
                    )
        csv_writing()

    elif type(ref) == utils.Flug_verif_coax:
        # rotors = [
        #     utils.Rotor(surfaces=[utils.Surface(1, 0), utils.Surface(2, 0), utils.Surface(
        #         3, 0)], pitch_axis="Y", rpm=0, rotation_axis="X", id=1),
        #     utils.Rotor(surfaces=[utils.Surface(4, 0), utils.Surface(5, 0), utils.Surface(
        #         6, 0)], pitch_axis="Y", rpm=0, rotation_axis="X", id=2)
        # ]
        # sense_of_rotation = [True, False]
        sense_of_rotation = [ref.upper.ccw, ref.lower.ccw]
        rotors = [
            utils.Rotor(surfaces=[utils.Surface(1, 0)], pitch_axis="Y",
                        rpm=0, rotation_axis="X", id=1, ccw=sense_of_rotation[0]),
            utils.Rotor(surfaces=[utils.Surface(2, 0)], pitch_axis="Y",
                        rpm=0, rotation_axis="X", id=2, ccw=sense_of_rotation[1])
        ]  # simple rotor
        # rotors = [
        #     utils.Rotor(surfaces=[utils.Surface(1, 0), utils.Surface(2, 0)], pitch_axis="Y", rpm=0, rotation_axis="X", id=1),
        #     utils.Rotor(surfaces=[utils.Surface(3, 0), utils.Surface(4, 0)], pitch_axis="Y", rpm=0, rotation_axis="X", id=2)
        # ] # no hub

        rotors = [
            utils.Rotor(surfaces=[utils.Surface(1, 0), utils.Surface(2, 0), utils.Surface(
                3, 0)], pitch_axis="Y", rpm=0, rotation_axis="X", id=1, ccw=sense_of_rotation[0]),
            utils.Rotor(surfaces=[utils.Surface(4, 0), utils.Surface(5, 0), utils.Surface(
                6, 0)], pitch_axis="Y", rpm=0, rotation_axis="X", id=2, ccw=sense_of_rotation[1])
        ]

        # sets = tm_r.test_matrix_coax_rpm_only(rpms_a=ref.upper.rpms, rpms_b=ref.lower.rpms, j=4) # this is for combination not the case for validation
        # sets = [(0, 4, ref.upper.rpms[i], ref.lower.rpms[i]) for i in range(len(ref.upper.rpms))]

        freestream_velocity = 0.001
        sets = [(0, freestream_velocity, ref.upper.rpms[i], ref.lower.rpms[i])
                for i in range(len(ref.upper.rpms))]

        ws.flugauto_text_matrix_main(
            steady, dirname, flightstream_file, sets, rotors, d, ref_area, coax=fs_p.COAX, pitch=fs_p.PITCH, spacing=fs_p.SPACING)
        print("write script coax done")

        # # return # just write the script
        list_script_files = ls_outputs_txt(os.path.join(dirname, str_steady))
        fs_exe.main_list(dirname, list_script_files)

        total, upper, lower = out_ana.thrust_power_analysis_coax(
            dirname, steady, to_write=False)
        RPM2RAD = 2 * np.pi / 60
        # print(upper)#DEBUG
        # print("upper rpm: ", upper[0].rpm)
        # print("lower rpm: ", lower[0].rpm)
        # print(upper[0].fx)
        # print(upper, len(upper))
        # print(ref.upper.rpms, len(ref.upper.rpms))
        # print("Moment du sup:")#DEBUG
        # print(upper[0].mx, len(upper))
        # print("Moment de l'inf")
        # print(lower[0].mx, len(lower))
        # exp_upper = utils.Flug_verif(rpms=ref.upper.rpms, thrusts_newton=[ -upper[i].fx for i in range(len(upper))],
        #                              powers=[upper[i].power for i in range(len(ref.upper.rpms))])
        # exp_lower = utils.Flug_verif(rpms=ref.lower.rpms, thrusts_newton=[ -lower[i].fx for i in range(len(lower))],
        #                              powers=[lower[i].power for i in range(len(ref.lower.rpms))])

        # print("Len upper and ref.upper for power", len(upper), len(ref.upper.rpms))#DEBUG
        # print("Len lower and ref.lower for power", len(lower), len(ref.lower.rpms))
        upper_rpms = []
        upper_thrusts_newton = []
        upper_powers = []
        upper_torques = []
        lower_rpms = []
        lower_thrusts_newton = []
        lower_powers = []
        lower_torques = []

        sign_upper = 1
        sign_lower = 1
        if not ref.upper.ccw:
            sign_upper = -1
        if not ref.lower.ccw:
            sign_lower = -1
        for i in range(len(upper)):
            upper_rpms.append(upper[i].rpm)
            upper_thrusts_newton.append(-upper[i].fx)

            lower_rpms.append(lower[i].rpm)
            lower_thrusts_newton.append(-lower[i].fx)

            upper_torques.append(sign_upper * upper[i].mx)
            upper_powers.append(
                sign_upper * upper[i].mx*upper[i].rpm * RPM2RAD)

            lower_torques.append(sign_lower * lower[i].mx)
            lower_powers.append(
                sign_lower * lower[i].mx*lower[i].rpm * RPM2RAD)

        exp_upper = utils.Flug_verif(rpms=upper_rpms,
                                     thrusts_newton=upper_thrusts_newton,
                                     powers=upper_powers,
                                     torques=upper_torques,
                                     ccw=sense_of_rotation[0])
        # TODO adapt to have not the same rpms for the reference and the exp/calculation
        exp_lower = utils.Flug_verif(rpms=lower_rpms,
                                     thrusts_newton=lower_thrusts_newton,
                                     powers=lower_powers,
                                     torques=lower_torques,
                                     ccw=sense_of_rotation[1])

        exp = utils.Flug_verif_coax(exp_upper, exp_lower)

        # # plotting
        pre_title = "[upper-A_rotor] "
        # flug_plot(ref.upper, exp.upper, pre_title)
        pre_title = "[lower-B_rotor] "
        # flug_plot(ref.lower, exp.lower, pre_title)

        # ref_tot = utils.Flug_verif(rpms = [str(ref.upper.rpms[i]) + " / " + str(ref.lower.rpms[i]) for i in range(len(ref.upper.rpms))],
        #                             thrusts_kgf = [ref.lower.thrusts_kgf[i] + ref.upper.thrusts_kgf[i] for i in range(len(ref.upper.thrusts_kgf))],
        #                             powers = [ref.lower.powers[i] + ref.upper.powers[i] for i in range(len(ref.upper.powers))])
        # exp_tot = utils.Flug_verif(rpms = [str(exp.lower.rpms[i]) + " / " + str(exp.upper.rpms[i]) for i in range(len(exp.upper.rpms))],
        #                             thrusts_kgf = [exp.lower.thrusts_kgf[i] + exp.upper.thrusts_kgf[i] for i in range(len(exp.upper.thrusts_kgf))],
        #                             powers = [exp.lower.powers[i] + exp.upper.powers[i] for i in range(len(exp.upper.powers))])
        ref_tot = utils.Flug_verif(rpms=ref.upper.rpms,
                                   thrusts_kgf=[ref.lower.thrusts_kgf[i] + ref.upper.thrusts_kgf[i]
                                                for i in range(len(ref.upper.thrusts_kgf))],
                                   powers=[ref.lower.powers[i] + ref.upper.powers[i]
                                           for i in range(len(ref.upper.powers))],
                                   torques=[ref.lower.torques[i] + ref.upper.torques[i]
                                            for i in range(len(ref.upper.torques))],
                                   ccw=sense_of_rotation[0])
        exp_tot = utils.Flug_verif(rpms=exp.upper.rpms,
                                   thrusts_kgf=[exp.lower.thrusts_kgf[i] + exp.upper.thrusts_kgf[i]
                                                for i in range(len(exp.upper.thrusts_kgf))],
                                   powers=[exp.lower.powers[i] + exp.upper.powers[i]
                                           for i in range(len(exp.upper.powers))],
                                   torques=[exp.lower.torques[i] + exp.upper.torques[i]
                                            for i in range(len(exp.upper.torques))],
                                   ccw=sense_of_rotation[1])
        pre_title = "[A+B rotors] "
        # flug_plot(ref_tot, exp_tot, pre_title)

        def csv_writing_old(str_identifier=""):
            steady_str = "steady"
            if not steady:
                steady_str = "un" + steady_str
            with open(os.path.join(dirname, os.path.join(steady_str, str_identifier + "coax_rotor_data.csv")), "w+") as f:  # TODO add specific name
                # TODO WARNING RECHANGE KGF TO NEWTON
                f.write(
                    "ref A rpm, ref A thrust (kgf), ref A torques (N.m), ref A power (W), exp A thrust (kgf), exp A torques (N.m), exp A power (W), rel A diff thrust, rel A diff power, " +
                    "ref B rpm, ref B thrust (kgf), ref B torques (N.m), ref B power (W), exp B thrust (kgf), exp B torques (N.m), exp B power (W), rel B diff thrust, rel B diff power\n")
                for i in range(len(ref.upper.rpms)):
                    f.write(
                        f"{ref.upper.rpms[i]:5.0f}" + ", " +
                        f"{ref.upper.thrusts_kgf[i]:5.5f}" + ", " +
                        f"{ref.upper.torques[i]:5.5f}" + ", " +
                        f"{ref.upper.powers[i]:5.5f}" + ", " +
                        f"{exp.upper.thrusts_kgf[i]:5.5f}" + ", " +
                        f"{exp.upper.torques[i]:5.5f}" + ", " +
                        f"{exp.upper.powers[i]:5.5f}" + ", " +
                        f"{(ref.upper.thrusts_kgf[i]-exp.upper.thrusts_kgf[i])/ref.upper.thrusts_kgf[i]:1.5f}" + ", " +
                        f"{(ref.upper.powers[i]-exp.upper.powers[i])/ref.upper.powers[i]:1.5f}" + ", " +
                        f"{ref.lower.rpms[i]:5.0f}" + ", " +
                        f"{ref.lower.thrusts_kgf[i]:5.5f}" + ", " +
                        f"{ref.lower.torques[i]:5.5f}" + ", " +
                        f"{ref.lower.powers[i]:5.5f}" + ", " +
                        f"{exp.lower.thrusts_kgf[i]:5.5f}" + ", " +
                        f"{exp.lower.torques[i]:5.5f}" + ", " +
                        f"{exp.lower.powers[i]:5.5f}" + ", " +
                        f"{(ref.lower.thrusts_kgf[i]-exp.lower.thrusts_kgf[i])/ref.lower.thrusts_kgf[i]:1.5f}" + ", " +
                        f"{(ref.lower.powers[i]-exp.lower.powers[i])/ref.lower.powers[i]:1.5f}\n"
                    )

        def csv_writing_old_v2(str_identifier=""):
            steady_str = "steady"
            if not steady:
                steady_str = "un" + steady_str
            with open(os.path.join(dirname, os.path.join(steady_str, str_identifier + "coax_rotor_data.csv")), "w+") as f:  # TODO add specific name
                # TODO WARNING RECHANGE KGF TO NEWTON
                f.write(
                    "ref A rpm, ref A thrust (kgf), ref A torques (N.m), ref A power (W), exp A rpm, exp A thrust (kgf), exp A torques (N.m), exp A power (W), " +
                    "ref B rpm, ref B thrust (kgf), ref B torques (N.m), ref B power (W), exp B rpm, exp B thrust (kgf), exp B torques (N.m), exp B power (W)\n")
                for i in range(len(ref.upper.rpms)):
                    f.write(
                        f"{ref.upper.rpms[i]:5.0f}" + ", " +
                        f"{ref.upper.thrusts_kgf[i]:5.5f}" + ", " +
                        f"{ref.upper.torques[i]:5.5f}" + ", " +
                        f"{ref.upper.powers[i]:5.5f}" + ", " +
                        f"{exp.upper.rpms[i]:5.0f}" + ", " +
                        f"{exp.upper.thrusts_kgf[i]:5.5f}" + ", " +
                        f"{exp.upper.torques[i]:5.5f}" + ", " +
                        f"{exp.upper.powers[i]:5.5f}" + ", " +
                        f"{ref.lower.rpms[i]:5.0f}" + ", " +
                        f"{ref.lower.thrusts_kgf[i]:5.5f}" + ", " +
                        f"{ref.lower.torques[i]:5.5f}" + ", " +
                        f"{ref.lower.powers[i]:5.5f}" + ", " +
                        f"{exp.lower.rpms[i]:5.0f}" + ", " +
                        f"{exp.lower.thrusts_kgf[i]:5.5f}" + ", " +
                        f"{exp.lower.torques[i]:5.5f}" + ", " +
                        f"{exp.lower.powers[i]:5.5f}" + "\n"
                    )
        # TODO sort the data to have one line

        def csv_writing(str_identifier=""):
            """save only the "exp" value, calculated by FlightStream, the reference is not written in it"""
            steady_str = "steady"
            if not steady:
                steady_str = "un" + steady_str
            with open(os.path.join(dirname, os.path.join(steady_str, str_identifier + "coax_rotor_data.csv")), "w+") as f:  # TODO add specific name
                # TODO WARNING RECHANGE KGF TO NEWTON
                f.write(
                    "exp A rpm, exp A thrust (kgf), exp A torques (N.m), exp A power (W), " +
                    "exp B rpm, exp B thrust (kgf), exp B torques (N.m), exp B power (W)\n")
                for i in range(len(exp.upper.rpms)):
                    f.write(
                        f"{exp.upper.rpms[i]:5.0f}" + ", " +
                        f"{exp.upper.thrusts_kgf[i]:5.5f}" + ", " +
                        f"{exp.upper.torques[i]:5.5f}" + ", " +
                        f"{exp.upper.powers[i]:5.5f}" + ", " +
                        f"{exp.lower.rpms[i]:5.0f}" + ", " +
                        f"{exp.lower.thrusts_kgf[i]:5.5f}" + ", " +
                        f"{exp.lower.torques[i]:5.5f}" + ", " +
                        f"{exp.lower.powers[i]:5.5f}" + "\n"
                    )
        csv_writing()


if __name__ == "__main__":
    folder_path = ""
    steady = True
    periodic = 0
    naca = False
    flugauto = False
    simple_rotor = False
    coax_rotor = False
    opti = False
    ana_opti = False
    pickle_file = ""
    title = ""
    print("syys argv ", sys.argv)
    for arg in sys.argv:
        print("arg: ", arg)
        if arg.startswith("-main"):
            index = arg.find("=")
            if index != -1:
                if "coax-rotor" in arg[index+1:]:
                    coax_rotor = True
                elif "simple-rotor" in arg[index+1:]:
                    simple_rotor = True
                elif "naca" in arg[index+1:]:
                    naca = True
                elif "flug" in arg[index+1:]:
                    flugauto = True
                elif "opti" in arg[index+1:]:
                    opti = True
                elif "analysis" in arg[index+1:]:
                    ana_opti = True
            else:
                raise Exception
        if arg.startswith("-path"):
            index = arg.find("=")
            if index != -1:
                folder_path = arg[index+1:]
                print("folder path: ", folder_path)
            else:
                raise Exception
        if arg.startswith("-picklefile"):
            index = arg.find("=")
            if index != -1:
                pickle_file = arg[index+1:]
                print("folder path: ", pickle_file)
            else:
                raise Exception
        elif arg.startswith("-title"):
            index = arg.find("=")
            if index != -1:
                title = arg[index+1:]
                print("title ", title)
    dirname = os.path.join(os.path.dirname(__file__), folder_path)
    print("main dirname:", dirname)
    if naca:
        # folder_path = sys.argv[1]
        if "-steady" in sys.argv:
            steady = True
        elif "-unsteady" in sys.argv:
            steady = False
        naca_main(dirname, steady, periodic)
    elif flugauto:
        flugauto_main(dirname)
    elif simple_rotor:
        print("simple rotor")
        steady = False
        print("steady: ", steady)
        flugauto_validation_simple_rotor(dirname, steady)
    elif coax_rotor:
        print("coax rotor")
        #flugauto_coax_rotors_verification(dirname, steady)
        steady = False
        coax_flug_verif(dirname, steady)
    elif opti:
        print("optimization")
        fl_opt.main(dirname, None)
    elif ana_opti:
        print("analysis")
        if pickle_file != "":
            print(dirname)
            ext_dict = fl_opt.analysis(dirname, pickle_file + ".pkl")

            ext_dict["name"] = "Opti-"  # total_thrust if in newton
            ext_dict["thrust_unit"] = "newton"

            extract_data.setup(ext_dict, usual=False, pretitle=title)
        else:
            # ext_dict = fl_opt.analysis(dirname)

            flug_dict = loadmat("contour_map/flugauto.mat")
            flug_dict["name"] = "FL -"  # kgf
            flug_dict["thrust_unit"] = "kgf"

            fs_dict = loadmat("contour_map/flightstream.mat")
            fs_dict["name"] = "FS -"  # kgf
            print(fs_dict.keys())
            fs_dict["thrust_unit"] = "kgf"
            extract_data.setup(fs_dict, usual=True, d_flug=flug_dict)
