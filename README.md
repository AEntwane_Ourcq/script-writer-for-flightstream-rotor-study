# Aim

This repository was developed during an individual research project to automate the creation of FlightStream scripts for the study of a coaxial rotor.
This was done with the 2021-releases of FlightStream.

# Licence

This project is under the GNU licence v3.

# Dependency

To write the FlightStream script only **Python** is required.
To run the script, obviously, **FlightStream** is required (https://researchinflight.com), and the `boolean unite` function (optional) needs **OpenVSP** (http://openvsp.org/).

# Initialization

- Create in the folder `flightstream_files`:
    - the link `FlightStream.lnk` to the application, like `C:\Users\<User_name>\AppData\Roaming\Research in Flight\FlightStream\FlightStream.exe`
    - the text file `OpenVSP_path.txt` and write in it the path to the folder OpenVSP (before `vsp.exe`), like `C:\Users\<User_name>\.*\OpenVSP-<version>-win64\`
- Add in the folder `flightstream_files` the flightstream project `*.fsm` and complete the `NAME` variable of in `flightstream_project` (for some part of the code (related to the NACA 640 report) change the name in `main.py` of the constant `NACA_FLIGHTSTREAM_PROJECT`). Complete the other variables in the `flightstream_project` file.

# Utillisation

To use the python script, launch `python main.py` with the following arguments:
- `-steady` or `-unsteady` to use steady (rotary) solver mode or unsteady solver mode.
- `-main=` to choose between several modes of this programme: 
    - `coax-rotor`, 
    - `simple-rotor`, 
    - `naca`: simple rotor with rotational speed, pitch angle and freestream velocity j setup, 
    - `flug`: coax rotors, currently no freestream nor pitch angle, 
    - `opti`: run the Nelder Mead algorithm to optimize the Figure of Merit (or the ratio thrust / power consumed). Create the `pkl_save` folder to export the pickle save of the points calculated.
    - `analysis`: run the analysis of the `output` folder. For the optimisation part `contour_map/extract_data` is used to visualize the data.
- `-path=<relative_path_to_the_folder>` to specify where to write/read the files. It will create this tree 
    - for the `naca` study:
    ```
    (un)steady/
                scripts/angle_<value>/j_<value>.txt
                output/angle_<value>/
                                    j_<value>.txt
                                    img_<value>.bmp
    ```
    - for the optimisation `opti`:
    ```
    (un)steady/
                scripts/<name>.txt
                output/angle_<value>/
                                    <name>.txt
                                    <name>.bmp
    ```
    with the `<name>` will look like: `angle_XXXX_freestream_XXXX_prop_a_XXXX_prop_b_XXXX_pitch_a_XXXX_pitch_b_XXXX_periodic_spacing_XXXX`

    The image files (`bmp`) were used to confirmed that there was no flow separation (and therefore not a wrong estimation of the `Fx` and `Mx`).
- `-picklefile=<relative_path_to_file.pkl>`: specify which save file to read. Useful to increase the number of iterations without re-calculating (with FlightStream) the points already explored by NM but read it from the pickle-file as NM is a deterministic approach). The settings of the NM method and the FlightStream project (`.fsm` and `.py`) should not be changed to let the NM algorithm run via the same points.

# Genericity of the project

The most generic parts are `flightstream_commands`, `flightstream_execute` then `parser2`, `write_script` then the rest of the folder.
