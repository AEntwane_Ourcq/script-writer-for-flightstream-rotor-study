import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import griddata

# mpl.rcParams['text.usetex'] = True #LATEX


def grid_data(xs, ys, zs):
    if type(xs) == list:
        xs = np.array(xs)
    if type(ys) == list:
        ys = np.array(ys)
    xs_min = xs.min()
    xs_max = xs.max()
    ys_min = ys.min()
    ys_max = ys.max()

    # n = 500j  # complex j needed for grid interpret at number of points
    # grid_xs, grid_ys = np.mgrid[xs_min:xs_max:n, ys_min:ys_max:n] do not use... broken (make a rotation xs=ys)

    xs_step = (xs_max-xs_min)/100
    ys_step = (ys_max-ys_min)/100

    grid_xs, grid_ys = np.meshgrid(
        np.arange(xs_min, xs_max+xs_step, xs_step),
        np.arange(ys_min, ys_max+ys_step, ys_step))
    points = []
    for i in range(len(xs)):
        points.append([xs[i], ys[i]])
    grid = griddata(points, zs, (grid_xs, grid_ys), method='linear')
    return xs_min, xs_max, ys_min, ys_max, grid


def plot(xs, ys, grid, extent, fig, ax, title, xlabel, ylabel, cbar_unit, extremum_graph=None, nbins=1, cmap=None):
    ax.plot(xs, ys, "+k")  # plotting the calculated points
    nb = 10
    print(title, extremum_graph, xlabel, ylabel)
    if extremum_graph:
        min_plot, max_plot = extremum_graph[0], extremum_graph[1]
        dt = (max_plot-min_plot)/nb
        levels_z = np.arange(min_plot, max_plot+dt, dt)
        # if "Torque balance" in title:
        #     min_ind = int(round(nb * min_plot / (min_plot - max_plot),0))
        #     print("TORQUE balance : ", min_ind)
        #     if levels_z[min_ind]>0:
        #         levels_z_zero_ind = (levels_z[min_ind-1], levels_z[min_ind])
        #     else:
        #         levels_z_zero_ind = (levels_z[min_ind], levels_z[min_ind+1])
        #     CS = ax.contour(grid, levels=levels_z_zero_ind, colors=("orange",), linestyles=('-',),linewidths=(2,),  extent=extent)
        #     plt.clabel(CS, fmt = '%1.4f', colors = 'k', fontsize=11)
        #     # plt.contour(X, Y,log_mu,levels = [-7,-8],
        #     #         colors=('k',),linestyles=('-',),linewidths=(2,))
        #     # cs = ax.contourf(grid, levels=levels_z_zero_ind, cmap=cmap,
        #     #              extent=extent, origin=None, extend='both')

        cs = ax.contourf(grid, levels=levels_z, cmap=cmap,
                         extent=extent, origin=None, extend='both')
        if not cmap:
            cmap = mpl.cm.get_cmap("viridis").copy()
            cmap.set_over('red')
            cmap.set_under('violet')
        cs.set_cmap(cmap)
    else:
        cs = ax.contourf(grid, levels=nb, cmap=cmap,
                         extent=extent, origin=None)
        # cs = ax2.contourf(grid, levels=nb, cmap=plt.cm.viridis)
    # max min color example
    # cs = plt.contourf(grid, levels=[10, 30, 50],
    # colors=['#808080', '#A0A0A0', '#C0C0C0'], extend='both') # extend for the colobar to have arrow at the end
    # need to have extend=both and levels are specified (to have max_on_color_bar<real_max to use red portion/set_over color)

    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    # rpm_ticks = [i for i in range(2500,fs_p.ABS_MAX_RPM+500,500)]
    # pitch_ticks = [i/100 for i in range(0,15)]
    # if "RPM" in xlabel:
    #     ax.set_xticks(rpm_ticks)
    # elif "pitch" in xlabel:
    #     pass
    # if "RPM" in ylabel:
    #     ax.set_yticks(rpm_ticks)
    # elif "pitch" in ylabel:
    #     pass

    cbar = fig.colorbar(cs, ax=ax, shrink=0.9)
    cbar.ax.set_ylabel(cbar_unit)

    # Add the contour line levels to the colorbar
    # cs2 = ax2.contour(cs, levels=cs.levels[::2], colors='r', origin='lower')
    # cbar.add_lines(cs2)

    # plt.show()


def contour_map(xs, ys, zs, title=None, xlabel=None, ylabel=None, cbar_unit=None, extremum_graph=None):
    fig, ax = plt.subplots(constrained_layout=True)

    xs_min, xs_max, ys_min, ys_max, grid = grid_data(xs, ys, zs)
    extent = (xs_min, xs_max, ys_min, ys_max)
    plot(xs, ys, grid, extent, fig, ax, title,
         xlabel, ylabel, cbar_unit, extremum_graph)


def contour_map_graph(graph, saving=False):
    fig, axs = plt.subplots(1, 1, constrained_layout=True)
    cmap = mpl.cm.get_cmap("viridis").copy()
    cmap.set_over('red')
    cmap.set_under('darkviolet')
    xs_min, xs_max, ys_min, ys_max, grid = grid_data(
        graph.xs, graph.ys, graph.zs)
    extent = (xs_min, xs_max, ys_min, ys_max)

    plot(graph.xs, graph.ys, grid, extent, fig, axs, graph.title,
         graph.xs_label, graph.ys_label, graph.unit, graph.extremum, cmap=cmap)

    if saving:
        t = graph.title[:graph.title.index("-")].strip()
        # plt.savefig(t+"_plot_transparent_solo.png", transparent=True, dpi=300)
        plt.savefig(t+"_plot_solo.png", transparent=False, dpi=300)
    # else:
    #     plt.show()


def contour_map_list(graphs, saving=False):
    l, c = np.shape(graphs)
    fig, axs = plt.subplots(l, c, constrained_layout=True,
                            figsize=(10*c/1.5, 10*l/2))
    cmap = mpl.cm.get_cmap("viridis").copy()
    cmap.set_over('red')
    cmap.set_under('darkviolet')
    for i in range(l):
        for j in range(c):
            if isinstance(graphs[i, j], Empty_Graph):
                fig.delaxes(ax=axs[i, j])
                continue
            # print("axis: ", axs[i, j])
            # print(graphs[i,j].xs, graphs[i,j].ys, graphs[i,j].zs, graphs[i,j].title,
            #         graphs[i,j].xs_label, graphs[i,j].ys_label, graphs[i,j].unit, graphs[i,j].extremum)
            # print(graphs[i,j].title)
            xs_min, xs_max, ys_min, ys_max, grid = grid_data(
                graphs[i, j].xs, graphs[i, j].ys, graphs[i, j].zs)
            extent = (xs_min, xs_max, ys_min, ys_max)

            if not(l == 1 or c == 1):
                plot(graphs[i, j].xs, graphs[i, j].ys, grid, extent, fig, axs[i, j], graphs[i, j].title,
                     graphs[i, j].xs_label, graphs[i, j].ys_label, graphs[i, j].unit, graphs[i, j].extremum, nbins=l*c, cmap=cmap)
            else:
                if l == 1:
                    plot(graphs[i, j].xs, graphs[i, j].ys, grid, extent, fig, axs[j], graphs[i, j].title,
                         graphs[i, j].xs_label, graphs[i, j].ys_label, graphs[i, j].unit, graphs[i, j].extremum, nbins=l*c, cmap=cmap)
                else:
                    plot(graphs[i, j].xs, graphs[i, j].ys, grid, extent, fig, axs[i], graphs[i, j].title,
                         graphs[i, j].xs_label, graphs[i, j].ys_label, graphs[i, j].unit, graphs[i, j].extremum, nbins=l*c, cmap=cmap)

    if saving:
        t = graphs[0, 0].title[:graphs[0, 0].title.index("-")].strip()
        # plt.savefig(t+"_plot_transparent.png", transparent=True, dpi=300)
        plt.savefig(t+"_plot.png", transparent=False, dpi=300)
    # else:
    #     plt.show()


class My_graph():
    def __init__(self, xs, ys, zs, title, xs_label, ys_label, unit, extremum=None):
        def convert(xs):
            if type(xs) == type(np.array([[]])):
                # print("array not list")
                return xs.tolist()[0]
            else:
                return xs
        self.xs = convert(xs)
        self.ys = convert(ys)
        self.zs = convert(zs)
        self.title = title
        self.unit = unit
        self.xs_label = xs_label
        self.ys_label = ys_label
        self.extremum = extremum

    def print(self):
        print("xs: " + self.xs.__repr__() + "\n"
              "ys: " + self.ys.__repr__() + "\n"
              "zs: " + self.zs.__repr__() + "\n")
        # "title: " +  self.title.__repr__() + "\n"
        # "xs_label: " + self.xs_label.__repr__()  + "\n"
        # "ys_label: " +  self.ys_label.__repr__() + "\n"
        # "unit: " +  self.unit.__repr__())

    def simple_plot(self):
        contour_map(self.xs, self.ys, self.zs, self.title,
                    self.xs_label, self.ys_label, self.unit, self.extremum)


class Empty_Graph(My_graph):
    def __init__(self):
        super().__init__([], [], [], "Empty graph", "", "", "", None)
