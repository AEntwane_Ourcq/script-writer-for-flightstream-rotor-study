import fnmatch
import os

import matplotlib.pyplot as plt
import numpy as np
import scipy.io
from fs_code import flightstream_project as fs_p
from fs_code import utils
from matplotlib.pyplot import xlabel, yscale
from scipy.io.matlab.mio import loadmat

from contour_map.plotting_map import (Empty_Graph, My_graph, contour_map,
                                      contour_map_graph, contour_map_list)


def read_mine_original(file):
    a_rpms = []
    b_rpms = []
    exp_a_thrusts = []
    exp_b_thrusts = []
    exp_a_powers = []
    exp_b_powers = []

    with open(file, "r") as f:
        c = 0
        for line in f:
            if c == 0:
                c += 1
                l = line.split(",")
                print(l[0], l[3], l[4], l[7], l[7+3], l[7+4])
            else:
                l = line.split(",")
                a_rpms.append(float(l[0]))
                exp_a_thrusts.append(float(l[3]))
                exp_b_thrusts.append(float(l[7+3]))
                exp_a_powers.append(float(l[4]))
                exp_b_powers.append(float(l[7+4]))
                b_rpms.append(-(float(l[0+7])))
    exp_a_torques = []
    exp_b_torques = []
    for i in range(len(exp_a_powers)):
        if not a_rpms[i] == 0:
            exp_a_torques.append(exp_a_powers[i] / (np.pi*a_rpms[i]/30))
        else:
            exp_a_torques.append(0)

        if not b_rpms[i] == 0:
            exp_b_torques.append(exp_b_powers[i] / (np.pi*b_rpms[i]/30))
        else:
            exp_b_torques.append(0)
    return a_rpms, b_rpms, exp_a_thrusts, exp_b_thrusts, exp_a_torques, exp_b_torques


def read_mine(file):
    a_rpms = []
    b_rpms = []
    exp_a_thrusts = []
    exp_b_thrusts = []
    exp_a_torques = []
    exp_b_torques = []

    # REDO index
    a_rpms_index = 0
    b_rpms_index = 0
    exp_a_thrusts_index = 0
    exp_b_thrusts_index = 0
    exp_a_torques_index = 0
    exp_b_torques_index = 0

    with open(file, "r") as f:
        c = 0
        for line in f:
            # print(line)
            if c == 0:
                c += 1
                l = line.split(", ")
                # print(l)
                a_rpms_index = l.index("exp A rpm")
                b_rpms_index = l.index("exp B rpm")
                exp_a_thrusts_index = l.index("exp A thrust (kgf)")
                exp_b_thrusts_index = l.index("exp B thrust (kgf)")
                exp_a_torques_index = l.index("exp A torques (N.m)")
                exp_b_torques_index = l.index("exp B torques (N.m)")
                # print(l[a_rpms_index], l[exp_a_thrusts_index], l[exp_b_thrusts_index],
                #      l[exp_a_torques_index], l[exp_b_torques_index], l[b_rpms_index])

            else:
                l = line.split(",")
                if True:  # not float(l[b_rpms_index]) == XXXX:
                    # remove aberrant point
                    a_rpms.append(float(l[a_rpms_index]))
                    exp_a_thrusts.append(float(l[exp_a_thrusts_index]))
                    exp_b_thrusts.append(float(l[exp_b_thrusts_index]))
                    exp_a_torques.append(float(l[exp_a_torques_index]))
                    exp_b_torques.append(float(l[exp_b_torques_index]))
                    b_rpms.append(float(l[b_rpms_index]))
                else:
                    print("REMOVING THE POINT :", float(
                        l[a_rpms_index]), float(l[b_rpms_index]))
    return a_rpms, b_rpms, exp_a_thrusts, exp_b_thrusts, exp_a_torques, exp_b_torques


def read_flug_original(file):
    a_rpms = []
    b_rpms = []
    a_thrusts = []
    b_thrusts = []
    a_torques = []
    b_torques = []
    with open(file, "r") as f:
        c = 0
        for line in f:
            if c == 0:
                c += 1
                l = line.split(",")
                print(l[9], l[6], l[5], l[19], l[16], l[15])
            else:
                l = line.split(",")
                a_rpms.append(float(l[9]))
                a_thrusts.append(float(l[6]))
                b_thrusts.append(float(l[16]))
                a_torques.append(float(l[5]))
                b_torques.append(float(l[15]))
                b_rpms.append(float(l[19]))

    return a_rpms, b_rpms, a_thrusts, b_thrusts, a_torques, b_torques


def read_flug(file):
    a_rpms = []
    b_rpms = []
    a_thrusts = []
    b_thrusts = []
    a_torques = []
    b_torques = []

    # REDO the index
    a_rpms_index = 0
    b_rpms_index = 0
    a_thrusts_index = 0
    b_thrusts_index = 0
    a_torques_index = 0
    b_torques_index = 0

    with open(file, "r") as f:
        c = 0
        for line in f:
            if c == 0:
                c += 1
                line = line.strip("\n")
                l = line.split(",")
                # print(l)
                # Motor Optical Speed A (RPM)	Thrust A (kgf)	Torque A (N·m)	Motor Optical Speed B (RPM)	Thrust B (kgf)	Torque B (N·m)
                a_rpms_index = l.index("Motor Optical Speed A (RPM)")
                b_rpms_index = l.index("Motor Optical Speed B (RPM)")
                a_thrusts_index = l.index("Thrust A (kgf)")
                b_thrusts_index = l.index("Thrust B (kgf)")
                a_torques_index = l.index("Torque A (N·m)")
                b_torques_index = l.index("Torque B (N·m)")

                # print(a_rpms_index, a_thrusts_index, b_thrusts_index,
                #       a_torques_index, b_torques_index, b_rpms_index)
                # print(l[a_rpms_index], l[a_thrusts_index], l[b_thrusts_index],
                #       l[a_torques_index], l[b_torques_index], l[b_rpms_index])
            else:
                l = line.split(",")
                a_rpms.append(float(l[a_rpms_index]))
                a_thrusts.append(float(l[a_thrusts_index]))
                b_thrusts.append(float(l[b_thrusts_index]))
                a_torques.append(float(l[a_torques_index]))
                b_torques.append(float(l[b_torques_index]))
                b_rpms.append(float(l[b_rpms_index]))

    return a_rpms, b_rpms, a_thrusts, b_thrusts, a_torques, b_torques


def main(savefile, files, mine):
    global_a_rpms, global_b_rpms, global_exp_a_thrusts, global_exp_b_thrusts, global_exp_a_torques, global_exp_b_torques = [], [], [], [], [], []
    for file in files:
        if mine:
            a_rpms, b_rpms, exp_a_thrusts, exp_b_thrusts, exp_a_torques, exp_b_torques = read_mine(
                file)
        else:
            a_rpms, b_rpms, exp_a_thrusts, exp_b_thrusts, exp_a_torques, exp_b_torques = read_flug(
                file)
        global_a_rpms = global_a_rpms[:] + a_rpms
        global_b_rpms = global_b_rpms[:] + b_rpms
        global_exp_a_thrusts = global_exp_a_thrusts[:] + exp_a_thrusts
        global_exp_b_thrusts = global_exp_b_thrusts[:] + exp_b_thrusts
        global_exp_a_torques = global_exp_a_torques[:] + exp_a_torques
        global_exp_b_torques = global_exp_b_torques[:] + exp_b_torques

    d = dict(a_rpms=global_a_rpms, b_rpms=global_b_rpms, a_thrusts=global_exp_a_thrusts,
             b_thrusts=global_exp_b_thrusts, a_torques=global_exp_a_torques, b_torques=global_exp_b_torques)

    # print(len(global_a_rpms), global_a_rpms)
    # print(min(global_a_rpms))
    # print(min(global_b_rpms))
    # print(max(global_a_rpms))
    # print(max(global_b_rpms))

    # scipy.io.savemat(savefile, d)
    return d


def previous(mine=True):
    if mine:

        # files = ["my_data/comparison/old_upper/coax_rotor_data.csv"]
        # files = ["my_data/comparison/new_upper/coax_rotor_data.csv"]

        # files = ["my_data/A_sweep_coax_rotor_data.csv"]
        files = ["my_data/v1_repaired/coax_rotor_data.csv"]
        main('flightstream.mat', files, True)
    else:
        def ls_outputs_txt(dirname="."):
            outputs = []
            output_str = os.path.join(dirname, "flug_data")
            list_of_files = os.listdir(output_str)
            csv_pattern = "*.csv"
            for file in list_of_files:
                if fnmatch.fnmatch(file, csv_pattern):
                    outputs.append(os.path.join(output_str, file))
            return outputs

        # flugauto files version
        # files = ls_outputs_txt()
        # main("flugauto.mat", files, mine)

        # files = ["my_data/v2/reference.csv"]
        # files = ["my_data/v2/merging_all_data_purged.csv"]
        files = ["my_data/v2/merging_all_data.csv"]
        main("flugauto.mat", files, False)


def coeff_fom_list(d_f, kgf=True):
    a_thrusts = np.array(d_f["a_thrusts"])
    b_thrusts = np.array(d_f["b_thrusts"])
    if kgf:
        KGF2N = 1/utils.N2KGF
        a_thrusts = a_thrusts*KGF2N
        b_thrusts = b_thrusts*KGF2N
    # total_foms = np.sqrt((total_thrusts*KGF2N)**3 / (rho * 2 * np.pi)) / (total_powers * fs_p.DIAMETER/2)
    # total_foms_new = (np.sqrt((np.array(d_f["a_thrusts"])*KGF2N)**3 / (rho * 2 * np.pi)) + np.sqrt((np.array(d_f["b_thrusts"])*KGF2N)**3 / (rho * 2 * np.pi)) ) / (total_powers * fs_p.DIAMETER/2)
    rho = 1.225
    np_a_rpms = np.array(d_f["a_rpms"])
    np_b_rpms = np.array(d_f["b_rpms"])
    print(np.shape(np_a_rpms))
    print(np.shape(np_b_rpms))
    print(np_a_rpms)
    print("old shape a rpoms")
    # b precaution (0 rpm):
    B_PRECAUTION = False
    if B_PRECAUTION:
        def coef_precaution(rpms, thrusts, torques):
            ct_i_list = []
            cp_i_list = []
            try:
                _, n = np.shape(rpms)
                for i in range(n):
                    if rpms[0, i] == 0:
                        ct_i_list.append(0)
                        cp_i_list.append(0)
                    else:
                        if thrusts[0, i] <= 0:
                            ct_i_list.append(0)
                        else:

                            ct_i_list.append(
                                thrusts[0, i] / (rho * (rpms[0, i]/60)**2 * (fs_p.DIAMETER)**4))
                        if torques[0, i] <= 0:
                            cp_i_list.append(0)
                        else:
                            cp_i_list.append((torques[0, i] * rpms[0, i] * np.pi /
                                              30) / (rho * (rpms[0, i]/60)**3 * (fs_p.DIAMETER)**5))
                        if np.isnan(ct_i_list[i]):
                            print(thrusts[0, i])
                            print(rpms[0, i])
                        elif np.isnan(cp_i_list[i]):
                            print(torques[0, i])
                            print(rpms[0, i])
            except:
                print("in excep")
                # rpms = np.array([rpms.tolist()])
                # thrusts = np.array([thrusts])
                # torques = np.array([torques])
                n = len(rpms.tolist())
                # print(thrusts)
                print(n)
                for i in range(n):
                    if rpms[i] == 0:
                        ct_i_list.append(0)
                        cp_i_list.append(0)
                    else:
                        if thrusts[i] <= 0:
                            ct_i_list.append(0)
                        else:

                            ct_i_list.append(
                                thrusts[i] / (rho * (rpms[i]/60)**2 * (fs_p.DIAMETER)**4))
                        if torques[i] <= 0:
                            cp_i_list.append(0)
                        else:
                            cp_i_list.append((torques[i] * rpms[i] * np.pi /
                                              30) / (rho * (rpms[i]/60)**3 * (fs_p.DIAMETER)**5))
                        if np.isnan(ct_i_list[i]):
                            print(thrusts[i])
                            print(rpms[i])
                        elif np.isnan(cp_i_list[i]):
                            print(torques[i])
                            print(rpms[i])

            ct_i = np.array([ct_i_list])
            cp_i = np.array([cp_i_list])
            return ct_i, cp_i
        ct_a, cp_a = coef_precaution(np_a_rpms, a_thrusts, d_f["a_torques"])
        ct_b, cp_b = coef_precaution(np_b_rpms, b_thrusts, d_f["b_torques"])
    else:
        ct_a = a_thrusts / (rho * (np_a_rpms/60)**2 * (fs_p.DIAMETER)**4)
        cp_a = (np.array(d_f["a_torques"]) * np_a_rpms * np.pi /
                30) / (rho * (np_a_rpms/60)**3 * (fs_p.DIAMETER)**5)
        ct_b = b_thrusts / (rho * (np_b_rpms/60)**2 * (fs_p.DIAMETER)**4)
        cp_b = (np.array(d_f["b_torques"]) * np_b_rpms * np.pi /
                30) / (rho * (np_b_rpms/60)**3 * (fs_p.DIAMETER)**5)
    cp = cp_a + cp_b
    if B_PRECAUTION:
        fom_list = []
        _, n = np.shape(cp)
        for i in range(n):
            if cp[0, i] == 0:
                fom_list.append(0)
            else:
                fom_list.append(
                    1.2657/np.sqrt(2) * (ct_a[0, i] **
                                         (3/2) + ct_b[0, i]**(3/2))/cp[0, i]
                )
                if np.isnan(1.2657/np.sqrt(2) * (ct_a[0, i]**(3/2) + ct_b[0, i]**(3/2))/cp[0, i]):
                    print(ct_a[0, i])
                    print(ct_b[0, i])
                    print(cp[0, i])
        total_foms = np.array([fom_list])
    else:
        total_foms = 1.2657/np.sqrt(2) * (ct_a**(3/2) + ct_b**(3/2))/cp

    # l,c = np.shape(total_foms)
    # # for i in range(l):
    # for j in range(c):
    #     #np_b_rpms[0,j]==0 or
    #     if  np.isnan(total_foms[0,j]):#==np.nan:
    #             print(j)
    #             print("There is at least one Not a Number FOM")
    #             break
    # # total_foms_old = 1/np.sqrt(2) * (ct_a + ct_b)**(3/2)/cp
    return total_foms


def adding_total(d_f):
    # if type(d_f["a_thrusts"]) == np.ndarray:
    np_a_rpms = np.array(d_f["a_rpms"])
    np_b_rpms = np.array(d_f["b_rpms"])
    total_thrusts = np.array(d_f["a_thrusts"]) + np.array(d_f["b_thrusts"])
    total_torques = np.array(d_f["a_torques"]) + np.array(d_f["b_torques"])
    total_torques_balance = np.array(
        d_f["a_torques"]) - np.array(d_f["b_torques"])
    total_powers = (np.array(d_f["a_torques"]) * np_a_rpms +
                    np.array(d_f["b_torques"]) * np_b_rpms) * np.pi/30
    total_kgf_w = total_thrusts / total_powers
    total_gramf_w = total_kgf_w * 1000

    # if kgf:
    #     KGF2N = 1/utils.N2KGF
    #     total_foms = np.sqrt((total_thrusts*KGF2N)**3 / (rho * 2 * np.pi)) / (total_powers * fs_p.DIAMETER/2)
    #     # total_foms_new = (np.sqrt((np.array(d_f["a_thrusts"])*KGF2N)**3 / (rho * 2 * np.pi)) + np.sqrt((np.array(d_f["b_thrusts"])*KGF2N)**3 / (rho * 2 * np.pi)) ) / (total_powers * fs_p.DIAMETER/2)
    #     ct_a = ct_a*KGF2N
    #     ct_b = ct_b*KGF2N

    # else:
    #     total_foms = np.sqrt((total_thrusts)**3 / (rho * 2 * np.pi)) / (total_powers * fs_p.DIAMETER/2)
    #    # total_foms_new = (np.sqrt((np.array(d_f["a_thrusts"]))**3 / (rho * 2 * np.pi)) + np.sqrt((np.array(d_f["b_thrusts"]))**3 / (rho * 2 * np.pi)) ) / (total_powers * fs_p.DIAMETER/2)
    # else:
    #     total_thrusts = (d_f["a_thrusts"]+ d_f["b_thrusts"])[0]
    #     total_torques = (d_f["a_torques"]+ d_f["b_torques"])[0]
    # total_foms = 1/np.sqrt(2) * (ct_a + ct_b)**(3/2)/cp
    total_foms = coeff_fom_list(d_f)
    # bug while converting array to list if already np array, [[]] and not []
    if len(total_thrusts) == 1:  # TODO clean this bad management of list and array
        d_f["total_thrusts"] = total_thrusts.tolist()[0]
        d_f["total_torques"] = total_torques.tolist()[0]
        d_f["total_torques"] = total_torques.tolist()[0]
        d_f["total_kgf_w"] = total_kgf_w.tolist()[0]
        d_f["total_gramf_w"] = total_gramf_w.tolist()[0]
        d_f["total_foms"] = total_foms.tolist()[0]
        d_f["total_powers"] = total_powers.tolist()[0]
        # d_f["total_foms_new"] = total_foms_new.tolist()[0]
        d_f["total_torques_balance"] = total_torques_balance.tolist()[0]
    else:
        d_f["total_thrusts"] = total_thrusts.tolist()
        d_f["total_torques"] = total_torques.tolist()
        d_f["total_torques"] = total_torques.tolist()
        d_f["total_kgf_w"] = total_kgf_w.tolist()
        d_f["total_gramf_w"] = total_gramf_w.tolist()
        d_f["total_foms"] = total_foms.tolist()
        d_f["total_powers"] = total_powers.tolist()
        # d_f["total_foms_new"] = total_foms_new.tolist()
        d_f["total_torques_balance"] = total_torques_balance.tolist()
    # print(total_thrusts)
    # print(total_thrusts.tolist()[0])

    return d_f


def ecart(d_fs, d_fl):
    a_rpms = []
    b_rpms = []
    a_thrusts = []
    b_thrusts = []
    a_torques = []
    b_torques = []

    # print(d_fl.keys())
    # for key in d_fl.keys():
    #     print(len(d_fl[key]))

    total_thrusts = []
    total_torques = []
    total_powers = []
    total_torques_balance = []
    total_foms = []

    if isinstance(d_fs["a_rpms"], np.ndarray):
        d_fs["a_rpms"] = d_fs["a_rpms"][0].tolist()
        d_fs["b_rpms"] = d_fs["b_rpms"][0].tolist()
        d_fs["a_thrusts"] = d_fs["a_thrusts"][0].tolist()
        d_fs["b_thrusts"] = d_fs["b_thrusts"][0].tolist()
        d_fs["a_torques"] = d_fs["a_torques"][0].tolist()
        d_fs["b_torques"] = d_fs["b_torques"][0].tolist()
        # d_fs["b_torques"] = d_fs["b_torques"][0].tolist()
        # d_fs["total_thrusts"] = d_fs["total_thrusts"][0].tolist()
        # d_fs["total_torques"] = d_fs["total_torques"][0].tolist()

        d_fl["a_rpms"] = d_fl["a_rpms"][0].tolist()
        d_fl["b_rpms"] = d_fl["b_rpms"][0].tolist()
        d_fl["a_thrusts"] = d_fl["a_thrusts"][0].tolist()
        d_fl["b_thrusts"] = d_fl["b_thrusts"][0].tolist()
        d_fl["a_torques"] = d_fl["a_torques"][0].tolist()
        d_fl["b_torques"] = d_fl["b_torques"][0].tolist()
        # total_powers = np.array(total_powers)
        # d_fl["total_torques_balance"] = d_fl["total_torques_balance"]
    # else:
    print(type(d_fs["a_rpms"]))
    for i in range(len(d_fs["a_rpms"])):
        for j in range(len(d_fl["a_rpms"])):
            if d_fs["a_rpms"][i] == d_fl["a_rpms"][j] and d_fs["b_rpms"][i] == d_fl["b_rpms"][j]:
                a_rpms.append(d_fl["a_rpms"][j])
                b_rpms.append(d_fl["b_rpms"][j])
                a_thrusts.append(d_fl["a_thrusts"][j])
                b_thrusts.append(d_fl["b_thrusts"][j])
                a_torques.append(d_fl["a_torques"][j])
                b_torques.append(d_fl["b_torques"][j])
                total_thrusts.append(d_fl["total_thrusts"][j])
                total_torques.append(d_fl["total_torques"][j])
                total_torques_balance.append(d_fl["total_torques_balance"][j])
                total_powers.append(d_fl["total_powers"][j])
                total_foms.append(d_fl["total_foms"][j])

                # total_thrusts.append(d_fl["a_thrusts"][j] + d_fl["b_thrusts"][j])
                # total_torques.append(d_fl["a_torques"][j] + d_fl["b_torques"][j])
                # total_powers.append((d_fl["a_torques"][j] * d_fl["a_rpms"][j] +
                # d_fl["b_torques"][j] * d_fl["b_rpms"][j]) * np.pi/30)
    d_fl_extract = dict(a_rpms=a_rpms, b_rpms=b_rpms, a_thrusts=a_thrusts,
                        b_thrusts=b_thrusts, a_torques=a_torques, b_torques=b_torques, total_thrusts=total_thrusts, total_torques=total_torques, total_torques_balance=total_torques_balance, total_foms=total_foms, total_powers=total_powers)

    if len(d_fs["a_rpms"]) != len(d_fl_extract["a_rpms"]):
        print("not the same length", len(
            d_fs["a_rpms"]), len(d_fl_extract["a_rpms"]))
        print("extraction failed...")
        if not len(d_fl_extract["a_rpms"]):
            print(d_fl_extract)
        raise Exception

    # checking order
    not_ordered = False
    for i in range(len(d_fs["a_rpms"])):
        if not (d_fs["a_rpms"][i] == d_fl_extract["a_rpms"][i] and d_fs["b_rpms"][i] == d_fl_extract["b_rpms"][i]):
            not_ordered = True
            print("needing reorder!")
            raise Exception

    ec_a_thrusts = []
    ec_b_thrusts = []
    ec_a_torques = []
    ec_b_torques = []
    ec_total_thrusts = []
    ec_total_torques = []
    ec_total_torques_balance = []
    ec_fom = []
    ec_total_powers = []
    if not not_ordered:
        def rel_dif_factor(d_ref, d_exp, label, index):
            return (d_exp[label][index] - d_ref[label][index])/d_ref[label][index]

        for i in range(len(d_fs["a_rpms"])):
            ec_a_thrusts.append(rel_dif_factor(
                d_fl_extract, d_fs, "a_thrusts", i))
            ec_b_thrusts.append(rel_dif_factor(
                d_fl_extract, d_fs, "b_thrusts", i))
            ec_a_torques.append(rel_dif_factor(
                d_fl_extract, d_fs, "a_torques", i))
            ec_b_torques.append(rel_dif_factor(
                d_fl_extract, d_fs, "b_torques", i))

            ec_total_thrusts.append(rel_dif_factor(
                d_fl_extract, d_fs, "total_thrusts", i))
            ec_total_torques.append(rel_dif_factor(
                d_fl_extract, d_fs, "total_torques", i))
            ec_total_torques_balance.append(rel_dif_factor(
                d_fl_extract, d_fs, "total_torques_balance", i))
            ec_fom.append(rel_dif_factor(d_fl_extract, d_fs, "total_foms", i))
            ec_total_powers.append(rel_dif_factor(
                d_fl_extract, d_fs, "total_powers", i))
            # ec_fom.append(total_foms_new.tolist())

    # ec_a_thrusts = np.array(ec_a_thrusts)
    # ec_b_thrusts = np.array(ec_b_thrusts)
    # ec_a_torques = np.array(ec_a_torques)
    # ec_b_torques = np.array(ec_b_torques)
    # ec_total_thrusts = np.array(ec_total_thrusts)
    # ec_total_torques = np.array(ec_total_torques)
    d_ecart = dict(a_rpms=a_rpms, b_rpms=b_rpms, a_thrusts=ec_a_thrusts,
                   b_thrusts=ec_b_thrusts, a_torques=ec_a_torques, b_torques=ec_b_torques, total_thrusts=ec_total_thrusts, total_torques=ec_total_torques, total_foms=ec_fom, total_torques_balance=total_torques_balance, total_powers=ec_total_powers)
    d_ecart["name"] = "RD -"
    return d_ecart


def one_z_several_xy(d_f, xy_couples, xy_labels, z, z_unit, title):
    # pre_a = pre + " A "
    # pre_b = pre + " B "

    # if not extremum_dict:
    extremum_dict = {"a_thrusts": None, "b_thrusts": None, "a_torques": None,
                     "b_torques": None, "total_thrusts": None, "total_torques": None, "total_torques_balance": None, "total_powers": None}
    l, c, d = np.shape(xy_couples)
    if d != 2:
        print("graph -> 2d")
        raise Exception
    gs = np.empty((l, c), dtype=My_graph)
    # print(xy_couples)
    # print(xy_labels)
    for i in range(l):
        for j in range(c):
            x, y = xy_couples[i, j]
            xlab, ylab = xy_labels[i, j]
            g = My_graph(d_f[x], d_f[y], d_f[z], title,
                         xlab, ylab, z_unit)
            gs[i, j] = g
    # return graphs_list

    # l, c = np.shape(graphs)
    # for i in range(l):
    #     for j in range(c):
    #         #         graphs[i,j].extremum = (-1, 1)
    #         graphs[i,j].simple_plot()
    # plt.show()

    # print(np.shape(graphs))
    # l, c = np.shape(graphs)
    # print(l,c)

    contour_map_list(gs)
    plt.savefig("run_opt.png", dpi=200)
    # plt.show()


def plotting_usual(d_fs, d_flug):

    def one_set_plot(d_f, pre, extremum_dict=None, unit=True, xyplot="rpm"):
        thrust_unit = ""
        torque_unit = ""
        if unit:
            thrust_unit = "(kgf)"
            torque_unit = "(N.m)"

        a_rpm_label = 'A rotational speed (rpm)'
        b_rpm_label = 'B rotational speed (rpm)'
        thrust_title = "Thrust "
        torque_title = "Torque "
        torque_balance_title = "Torque balance "
        try:

            if not extremum_dict:
                extremum_dict = {"a_thrusts": None, "b_thrusts": None, "a_torques": None,
                                 "b_torques": None, "total_thrusts": None, "total_torques": None, "total_powers": None}
            pre_a = pre + " A "
            pre_b = pre + " B "

            def plotting(x=d_f["a_rpms"], y=d_f["b_rpms"], xlab=a_rpm_label, ylab=b_rpm_label):

                a_thrusts_graph = My_graph(x, y, d_f["a_thrusts"], pre_a + thrust_title,
                                           xlab, ylab, thrust_unit, extremum_dict["a_thrusts"])
                b_thrusts_graph = My_graph(x, y, d_f["b_thrusts"], pre_b + thrust_title,
                                           xlab, ylab, thrust_unit, extremum_dict["b_thrusts"])
                a_torques_graph = My_graph(x, y, d_f["a_torques"], pre_a + torque_title,
                                           xlab, ylab, torque_unit, extremum_dict["a_torques"])
                b_torques_graph = My_graph(x, y, d_f["b_torques"], pre_b + torque_title,
                                           xlab, ylab, torque_unit, extremum_dict["b_torques"])

                # import numpy as np

                pre_total = pre + " Total "

                total_thrusts_graph = My_graph(x, y, d_f["total_thrusts"], pre_total +
                                               thrust_title, xlab, ylab, thrust_unit, extremum_dict["total_thrusts"])
                total_torques_balance_graph = My_graph(x, y, d_f["total_torques_balance"], pre_total +
                                                       torque_balance_title, xlab, ylab, torque_unit, extremum_dict["total_torques_balance"])

                total_torques_graph = My_graph(x, y, d_f["total_torques"], pre_total +
                                               torque_title, xlab, ylab, torque_unit, extremum_dict["total_torques"])

                total_foms_graph = My_graph(
                    x, y, d_f["total_foms"], d_f["name"] + " Figure of Merit", xlab, ylab, "", extremum_dict["total_foms"])

                total_powers_graph = My_graph(
                    x, y, d_f["total_powers"], pre_total + "power", xlab, ylab, "(W)", extremum_dict["total_powers"])

                # graphs_list = [
                #     [a_thrusts_graph, b_thrusts_graph],
                #     [a_torques_graph, b_torques_graph],
                #     [total_thrusts_graph, total_torques_graph]
                #     ]
                # if "total_kgf_w" in d_f.keys():
                # graphs_list.append(
                #     [My_graph(x, y, d_f["ratio_kgf_powers"], pre_total +
                #     "RATIO Force by power", xlab, ylab, "(kgf/W)"),
                #     My_graph(x, y, d_f["total_kgf_w"], pre_total +
                #     "Force by power", xlab, ylab, "(kgf/W)")])#, extremum_dict["total_torques"])

                #[a_torques_graph, b_torques_graph],
                graphs_list = graphs_list = [
                    [a_thrusts_graph, a_torques_graph],

                    # [My_graph(x, y, d_f["ratio_kgf_powers"], pre_total +
                    # "RATIO Force by power", xlab, ylab, "(kgf/W)")
                    # ]]
                    # ,total_kgf_w

                    [b_thrusts_graph, b_torques_graph],
                    [total_thrusts_graph, total_torques_graph]
                    # ,total_torques_balance_graph

                    # [total_foms_graph,total_torques_graph] #total_powers_graph
                    # [My_graph(x, y, d_f["total_gramf_w"], d_f["name"] +
                    # "Force / power", xlab, ylab, "(gf/W)")
                    # [My_graph(x, y, d_f["total_foms_new"], d_f["name"] +
                    #           "New FoM", xlab, ylab, ""),
                    # My_graph(x, y, d_f["ratio_kgf_powers"], d_f["name"] +
                    # "ratio Force / power", xlab, ylab, "(kgf/W)")
                    # ]]
                    #  My_graph(x, y, d_f["total_foms"], d_f["name"] +
                    #           "Figure of Merit", xlab, ylab, "")]

                ]

                # print("LISTING")
                graphs_list = np.array(graphs_list)
                # print(graphs_list)
                return graphs_list

            # if xyplot == "rpm":
            #     graphs_list = plotting(d_f["a_rpms"],d_f["b_rpms"])
            # elif xyplot == "pitch":
            #     pass
            #     graphs_list = plotting(d_f["a_rpms"],d_f["a_pitchs"], "A rotational speed (RPM)", "A pitch (°)")
            # graphs_list = graphs_list[:] + plotting(d_f["b_rpms"],d_f["b_pitchs"], "B rotational speed (RPM)", "B pitch (°)")
            graphs_list = plotting(
                d_f["a_rpms"], d_f["b_rpms"], "A rotational speed (RPM)", "B rotational speed (RPM)")
            graphs = np.array(graphs_list)
            # print("graphs:")
            # print(graphs)

            # graphs  = np.array([
            #     [a_thrusts_graph, b_thrusts_graph],
            #     [total_thrusts_graph, total_torques_graph]
            #     ])

            # for g in graphs:
            #     g.simple_plot()

            l, c = np.shape(graphs)
            # for i in range(l):
            #     for j in range(c):
            #         #         graphs[i,j].extremum = (-1, 1)
            #         graphs[i,j].simple_plot()
            # plt.show()
            l, c = np.shape(graphs)
            print(l, c)
            contour_map_list(graphs, saving=True)
            # graphs[3,0].extremum = None
            # contour_map_graph(graphs[3,0], saving=True)
            # plt.show()

        except KeyError as e:
            print("Key error:", e)
            print(d_f.keys())
            print(extremum_dict.keys())

    fs_pre = "FS"
    flug_pre = "FLUG"

    # one_set_plot(d_fs, fs_pre, xyplot="pitch")
    if d_flug:
        # setting the extremum:
        def set_extremum(ext, d_ref, lab):
            ext[lab] = (min(d_ref[lab]), max(d_ref[lab]))
            return ext
        flug_extremum = {}
        for lab in ["a_thrusts", "b_thrusts", "a_torques", "b_torques", "total_thrusts", "total_torques", "total_torques_balance", "total_foms", "total_powers"]:
            if len(d_flug[lab]) == 1:
                # print("type 0")
                # print(type(d_flug[lab][0]))
                # print(np.shape(d_flug[lab][0]))

                min_flug = min(d_flug[lab][0])
                max_flug = max(d_flug[lab][0])
                if lab == "total_foms":
                    max_flug = max(d_fs[lab][0])
                # not working this:-> or not (type(min_flug) == np.float64 and type(max_flug) == np.float64):
                if not (type(min_flug) == float and type(max_flug) == float):
                    pass
                    # print("min and max type (should be float (or maybe int?)):")
                    # print(type(min_flug), min_flug)
                    # print(type(max_flug), max_flug)
            else:
                min_flug = min(d_flug[lab])
                max_flug = max(d_flug[lab])
                if lab == "total_foms":
                    max_flug = max(d_fs[lab])

            # print(min(d_flug[lab]))
            flug_extremum[lab] = (min_flug, max_flug)

        # flug_extremum["total_foms"] = (0,.5) # Remove aberrant point bottom left corner at 5 FOM

        one_set_plot(d_flug, d_flug["name"], flug_extremum)
        one_set_plot(d_fs, d_fs["name"], flug_extremum)
        d_ecart = ecart(d_fs, d_flug)

        extrem_ec = {}
        for lab in ["a_thrusts", "b_thrusts", "a_torques", "b_torques", "total_thrusts", "total_torques", "total_torques_balance", "total_foms", "total_powers"]:
            min_d_ec = min(d_ecart[lab])
            max_d_ec = max(d_ecart[lab])
            extrem_ec[lab] = (max((-1, min_d_ec)), min((1, max_d_ec)))
            if lab == "b_thrusts" or lab == "total_foms":
                # extrem_ec["b_thrusts"] = (-1,3)
                extrem_ec[lab] = (max((-1, min_d_ec)), min((3, max_d_ec)))

        one_set_plot(d_ecart, d_ecart["name"], extrem_ec, unit=False)
        fom_graph = My_graph(d_ecart["a_rpms"], d_ecart["b_rpms"], d_ecart["total_foms"], "RD -" +
                             "", "A rotational speed (RPM)", "B rotational speed (RPM)", "", extrem_ec)
        # fom_ext = {}
        # fom_ext = {-1,1}
        # contour_map_graph(fom_graph, False)

    else:
        one_set_plot(d_fs, d_fs["name"])
    # one_set_plot(d_flug, flug_pre)

    # 2D
    # plt.plot(d_ecart["a_rpms"], d_ecart["a_thrusts"], "b+",
    #          d_ecart["b_rpms"], d_ecart["a_thrusts"], "m.")
    # plt.figure()
    # plt.plot(d_ecart["a_rpms"], d_ecart["b_thrusts"], "b+",
    #          d_ecart["b_rpms"], d_ecart["b_thrusts"], "m.")

    # 3D
    # fig = plt.figure()
    # ax = plt.axes(projection='3d')
    # # ax.contour3D(X, Y, Z, 50, cmap='binary')
    # ax.set_xlabel('x')
    # ax.set_ylabel('y')
    # ax.set_zlabel('z')

    # ax.scatter3D(d_ecart["a_rpms"], d_ecart["b_rpms"], d_ecart["b_thrusts"])

    # plt.show()


def old_setup():
    files = ["my_data/v2/coax_rotor_data.csv"]
    d_fs = main('flightstream.mat', files, True)

    files = ["my_data/v2/merging_all_data.csv"]
    d_fl = main("flugauto.mat", files, False)

    d_fs = adding_total(d_fs)
    d_fl = adding_total(d_fl)

    d_ecart = ecart(d_fs, d_fl)

    # contour_map(d_fs["a_rpms"], d_fs["b_rpms"], d_fs["a_thrusts"])
    plotting_usual(d_fs, d_fl)


def setup(d={}, usual=True, d_flug=None, pretitle=""):
    print(d.keys())
    if d["thrust_unit"] == "newton":
        d["a_thrusts"] = [utils.N2KGF * t for t in d["a_thrusts"]]
        d["b_thrusts"] = [utils.N2KGF * t for t in d["b_thrusts"]]
    # return
    d = adding_total(d)
    if usual:
        # optimization runs:
        # from pickle load + analysis extraction give the mat

        # dirname = os.path.join(os.path.dirname(__file__))
        # d = fl_opt.analysis(dirname)
        # d = loadmat("../flightstream_opti.mat")
        d_flug = adding_total(d_flug)
        plotting_usual(d, d_flug)
    else:
        # Run plotting each graphs (x/y) for the optimization value
        thrust_unit = "(kgf)"
        torque_unit = "(N.m)"
        a_rpm_label = 'A rotational speed (rpm)'
        b_rpm_label = 'B rotational speed (rpm)'
        a_pitch_label = "A pitch angle (°)"
        b_pitch_label = "B pitch angle (°)"

        rel_rpms = []
        rel_spacing = []
        rel_pitchs = []
        rel_pitch_null = False
        if d["a_pitchs"][0] == 0:
            rel_pitchs = [0 for a in range(len(d["a_pitchs"]))]
            rel_pitch_null = True
        else:
            for i in range(len(d["b_rpms"])):
                rel_pitchs.append(100*(d["b_pitchs"][i]/d["a_pitchs"][i]-1))
        for i in range(len(d["b_rpms"])):
            rel_rpms.append(100*(d["b_rpms"][i]/d["a_rpms"][i]-1))
            rel_spacing.append(100 * (1 - d["spacings"][i]/(fs_p.DIAMETER/2)))
        d["relative_rpm"] = rel_rpms
        d["spacings"] = rel_spacing
        d["rel_pitchs"] = rel_pitchs
        print("relative rpm:")
        print(d["relative_rpm"])

        pitch_rel_label = r"relative pitch $\frac{p_B}{p_A}-1$ (%)"
        relative_label = r"relative rotational speed $\frac{\omega_B}{\omega_A}-1$ (%)"
        spacing_label = r"relative spacing $\frac{H}{D}$ (%)"

        relative_label = r"relative rotational speed between the rotors $\frac{\omega_B}{\omega_A}-1$ (%)"
        spacing_label = r"relative spacing between the rotors $\frac{H}{D}$ (%)"
        if rel_pitch_null:
            xy_couples = np.array([
                # [
                #     ("a_rpms", "b_rpms"),
                #     ("a_pitchs", "b_pitchs")
                # ],
                # [
                #    ("relative_rpm", "b_pitchs"),
                #    ("b_rpms", "b_pitchs")
                # ],
                [("relative_rpm", "b_pitchs"),
                    ("relative_rpm", "spacings"),
                    ("b_pitchs", "spacings")
                 ]
            ],
                dtype=tuple)
            xy_labels = np.array([
                # [
                #     (a_rpm_label, b_rpm_label),
                #     (a_pitch_label, b_pitch_label)
                # ],
                [
                    (relative_label, b_pitch_label),
                    (relative_label, spacing_label),
                    # (pitch_rel_label, spacing_label)
                    (b_pitch_label, spacing_label)
                ]
            ])
        else:
            xy_couples = np.array([
                # [
                #     ("a_rpms", "b_rpms"),
                #     ("a_pitchs", "b_pitchs")
                # ],
                # [
                #    ("relative_rpm", "b_pitchs"),
                #    ("b_rpms", "b_pitchs")
                # ],
                [("relative_rpm", "rel_pitchs"),
                    ("relative_rpm", "spacings"),
                    ("rel_pitchs", "spacings")]
            ],
                dtype=tuple)
            xy_labels = np.array([
                # [
                #     (a_rpm_label, b_rpm_label),
                #     (a_pitch_label, b_pitch_label)
                # ],
                [
                    (relative_label, pitch_rel_label),
                    (relative_label, spacing_label),
                    # (pitch_rel_label, spacing_label)
                    (pitch_rel_label, spacing_label)
                ]
            ])

        total_fom_bool = True
        if total_fom_bool:
            z = "total_foms"

            z_unit = ""
            print(pretitle)
            # if pretitle != "":
            #     title = pretitle + " - Figure of Merit"
            # else:
            title = "Figure of Merit Optimisation"
        else:
            z = "total_gramf_w"
            z_unit = "(gf/W)"
            if pretitle != "":
                title = pretitle + " - Thrust/Power ratio"
            else:
                title = "Thrust/Power ratio"
        one_z_several_xy(d, xy_couples, xy_labels, z, z_unit, title)

        def find_max(d):
            ind = d["total_foms"].index(max(d["total_foms"]))
            print(d["a_rpms"][ind],
                  d["b_rpms"][ind],
                  d["a_pitchs"][ind],
                  d["b_pitchs"][ind],
                  d["spacings"][ind],
                  d["total_thrusts"][ind],
                  d["total_torques"][ind],
                  d["total_kgf_w"][ind],
                  d["total_foms"][ind]
                  )
        # find_max(d)
